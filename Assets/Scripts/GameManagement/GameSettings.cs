﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class GameSettings : MonoBehaviour
{
    private static GameSettings instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;

            Screen.SetResolution(Screen.width / 2, Screen.height / 2, true);
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = NumericalValues.GameEnvironment.FrameRate;
            Cursor.visible = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
