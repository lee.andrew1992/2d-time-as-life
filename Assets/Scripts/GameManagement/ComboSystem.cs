﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ComboSystem : MonoBehaviour
{
    public TextMeshProUGUI comboCounter;
    public Slider comboResetTimerSlider;
    public Image comboResetTimerSliderFill;
    public float comboResetDuration;
    private int currentCombo;
    private int highestCombo;
    private float comboResetTimer;

    public Color[] comboColors;

    public int HighestCombo { get { return highestCombo; } }
    public int CurrentCombo { get { return currentCombo; } }

    // Update is called once per frame
    void Update()
    {
        comboCounter.text = currentCombo.ToString();
        
        if (GameManager.Instance.roundStarted)
        {
            comboResetTimer -= Time.deltaTime;

            SetComboCounterColor();
            comboResetTimerSlider.value = comboResetTimer / comboResetDuration;

            ToggleUi(true);

            if (comboResetTimer <= 0)
            {
                ResetCombo();
            }
        }
    }

    public void ResetTimer()
    {
        comboResetTimer = comboResetDuration;
    }

    public void ResetTimer(float addedTime)
    {
        comboResetTimer = Mathf.Min(comboResetTimer + addedTime, comboResetDuration);
    }

    public void AddToComboCounter()
    {
        ResetTimer();
        currentCombo++;

        if (currentCombo > highestCombo)
        {
            highestCombo = currentCombo;
        }
    }

    public void ResetCombo()
    {
        currentCombo = 0;
        comboResetTimer = 0;

        ToggleUi(false);
    }

    private void ToggleUi(bool state)
    {
        comboResetTimerSlider.gameObject.SetActive(state);
        comboCounter.gameObject.SetActive(state);
    }

    private void SetComboCounterColor()
    {
        int comboColorIndex = GetComboColorIndex();
        Color currColor = comboColors[comboColorIndex];

        CommonFunctions.SetTMPColor(comboCounter, currColor);

        comboResetTimerSliderFill.color = currColor;
    }

    public int GetComboColorIndex()
    {
        return Mathf.Min((int)(currentCombo / 10), comboColors.Length - 1);
    }
}
