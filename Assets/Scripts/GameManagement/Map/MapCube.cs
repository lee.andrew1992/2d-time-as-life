﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class MapCube : MonoBehaviour
{
    public bool transitioning = false; 

    private float randomOffset;
    public float scaleSpeed;
    public MeshRenderer thisRenderer;

    private bool disabled = false;
    private bool fadingIn = false;
    public bool beingControlled = false;

    private Color originalColor;

    public bool isDisabled { get { return disabled; } }
    public Vector3 defaultScale;

    private Collider2D thisCollider;
    //private Rigidbody2D rb;

    private ObstacleEntity_Trap trapLogic;
    private ObstacleEntity_Wall wallLogic;

    public enum TransitionTo
    { 
        Wall,
        Trap
    }

    Vector2 originalPosition;

    // Start is called before the first frame update
    void Start()
    {
        trapLogic = this.GetComponent<ObstacleEntity_Trap>();
        wallLogic = this.GetComponent<ObstacleEntity_Wall>();

        trapLogic.enabled = false;
        wallLogic.enabled = false;

        GameManager.Instance.mapController.mapCubes.Add(this);
        randomOffset = Random.Range(-1f, 1f);
        originalColor = GetColor();
        originalPosition = this.transform.position;
        defaultScale = this.transform.localScale;
        thisCollider = this.GetComponent<Collider2D>();
        thisCollider.enabled = false;
        //rb = this.GetComponent<Rigidbody2D>();
        //rb.bodyType = RigidbodyType2D.Static;
    }

    // Update is called once per frame
    void Update()
    {
        if (CommonFunctions.OutsideCamera(Camera.main, this.transform.position, Vector2.one))
        {
            thisRenderer.enabled = false;
            return;
        }

        thisRenderer.enabled = true;
        float sinValue = Mathf.Sin(Time.time);
        
        if (!disabled && !fadingIn && !beingControlled)
        {
            SetColorAlpha(Mathf.Max(Mathf.Abs(sinValue * randomOffset) + 0.1f, 0.2f) * 0.75f);
        }
        
        //this.transform.localScale = new Vector3(0.85f, 0.85f, sinValue + randomOffset);
    }

    public void Disable(float duration)
    {
        if (disabled)
        {
            return;
        }

        StartCoroutine(FadeIn(duration));
    }

    private IEnumerator FadeIn(float duration)
    {
        float originalAlpha = GetColor().a;
        SetColorAlpha(0);
        disabled = true;
        this.thisRenderer.enabled = false;
        this.transform.position = originalPosition;

        float currDuration = 0;
        while (currDuration < duration)
        {
            currDuration += Time.deltaTime;
            yield return null;
        }

        this.thisRenderer.enabled = true;
        disabled = false;
        
        StartCoroutine(_FadeIn(originalAlpha));
     }

    private IEnumerator _FadeIn(float originalAlpha)
    {
        fadingIn = true;
        while (GetColor().a < originalAlpha)
        {
            SetColorAlpha(GetColor().a + Time.deltaTime);
            
            yield return null;
        }
        SetColorAlpha(originalAlpha);
        fadingIn = false;
    }

    private void SetColorAlpha(float alpha)
    {
        Color currColor = GetColor();
        thisRenderer.material.SetColor(StringValues.ShaderVariable.Common.MainColor,
            new Vector4(currColor.r, currColor.g, currColor.b, alpha));
    }

    private Color GetColor()
    {
        return thisRenderer.material.GetColor(StringValues.ShaderVariable.Common.MainColor);
    }

    public void SetColor(Color newColor)
    {
        thisRenderer.material.SetColor(StringValues.ShaderVariable.Common.MainColor, newColor);
    }

    public void ChangeToBackground()
    {
        this.GetComponent<Collider2D>().isTrigger = false;
        transitioning = false;
        beingControlled = false;
        this.gameObject.layer = NumericalValues.Layers.BackGround;
        this.SetColor(originalColor);
        //rb.simulated = false;
        //rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        ResetPosition();

        thisCollider.enabled = false;
        thisCollider.isTrigger = false;

        //rb.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
        //rb.bodyType = RigidbodyType2D.Static;
        
        GameManager.Instance.obstacleController.wallCubes.Remove(this);

        gameObject.GetComponent<ObstacleEntity_Wall>().enabled = false;
        GameManager.Instance.pathFindingUpdateController.RequestUpdate();

        StartCoroutine(ResetFadeValue());
    }

    private IEnumerator ResetFadeValue()
    {
        while (thisRenderer.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) < 0.9f)
        {
            thisRenderer.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, thisRenderer.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) + Time.deltaTime * 2);

            yield return null;
        }

        thisRenderer.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, 1);
    }

    public void ResetPosition()
    {
        this.transform.position = originalPosition;
    }

    public void Transition(TransitionTo transitionTo, float delay, float shakeVariation, Color transitionColor, Color finishedColor)
    {
        StopAllCoroutines();

        beingControlled = true;
        SetColor(transitionColor);
        transitioning = true;
        StartCoroutine(CommonFunctions.Shake(this.transform, delay, shakeVariation, originalPosition));

        switch (transitionTo)
        { 
            case TransitionTo.Wall:
                StartCoroutine(TurnToWall(delay, finishedColor));
                break;

            case TransitionTo.Trap:
                float trapDuration = 2f;
                StartCoroutine(TurnToTrap(delay, trapDuration, finishedColor));
                break;
        }
    }

    private IEnumerator TurnToWall(float delay, Color newColor)
    {
        yield return new WaitForSeconds(delay);
        transitioning = false;
        SetColor(newColor);
        gameObject.layer = NumericalValues.Layers.ObstacleWall;
        wallLogic.enabled = true;
        wallLogic.InstantiateObstacle();

        thisCollider.enabled = true;

        GameManager.Instance.obstacleController.wallCubes.Add(this);
        //rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        //rb.bodyType = RigidbodyType2D.Dynamic;
        //rb.simulated = true;
        //rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    private IEnumerator TurnToTrap(float delay, float trapDuration, Color newColor)
    {
        yield return new WaitForSeconds(delay);
        transitioning = false;
        SetColor(newColor);
        trapLogic.enabled = true;
        gameObject.layer = NumericalValues.Layers.TrapWall;
        //rb.simulated = true;
        //rb.constraints = RigidbodyConstraints2D.FreezeAll;

        GameManager.Instance.obstacleController.wallCubes.Remove(this);

        thisCollider.enabled = true;
        thisCollider.isTrigger = true;
        //rb.bodyType = RigidbodyType2D.Dynamic;

        TransitionToBackground(trapDuration);
    }

    private IEnumerator ObstableToBackground(float delay)
    {
        yield return new WaitForSeconds(delay);

        while (this.thisRenderer.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) > 0.1f)
        {
            float currFadeValue = thisRenderer.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade);
            thisRenderer.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, currFadeValue - Time.deltaTime * 2);

            yield return null;
        }

        thisRenderer.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, 0);
        ChangeToBackground();
    }

    public void TransitionToBackground(float delay)
    {
        trapLogic.enabled = false;
        wallLogic.enabled = false;
        StartCoroutine(ObstableToBackground(delay));
    }
}
