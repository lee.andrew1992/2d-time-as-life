﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class MapGenerator : MonoBehaviour
{
    public GameObject gridCubePrefab;
    public int row, column;

    // Start is called before the first frame update
    void Start()
    {
        GameObject map = GameObject.FindGameObjectWithTag(StringValues.Tags.Map);
        if (map == null)
        {
            GameObject mapParent = new GameObject();
            DontDestroyOnLoad(mapParent);
            mapParent.name = StringValues.Tags.Map;
            mapParent.tag = StringValues.Tags.Map;
            mapParent.isStatic = true;

            for (int x = 0; x < row; x++)
            {
                for (int y = 0; y < column; y++)
                {
                    GameObject cube = GameObject.Instantiate(gridCubePrefab, mapParent.transform);
                    cube.transform.position = new Vector2(x - row / 2, y - column / 2);
                }
            }
        }
        else
        {
            foreach (Transform cube in map.transform)
            {
                GameManager.Instance.mapController.mapCubes.Add(cube.GetComponent<MapCube>());
            }
        }
    }
}
