﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class MapController : MonoBehaviour
{
    public List<MapCube> mapCubes;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public MapCube GetClosestCubeInFront(Vector2 movementDir)
    { 
        List<MapCube> dupMapCubes = CommonFunctions.SortListByDistance<MapCube>((Vector2)GameManager.Instance.player.transform.position + movementDir, mapCubes);

        return dupMapCubes[0];
    }
}
