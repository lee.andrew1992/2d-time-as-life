﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopMenu : MonoBehaviour
{
    public GameObject shopInitial;
    public GameObject shopHome;

    public GameObject meleeUpgrades;
    public GameObject rangedUpgrades;
    public GameObject characterUpgrades;

    public GameObject descriptionBox;

    public void OpenShopScreen()
    {
        shopInitial.SetActive(false);
        shopHome.SetActive(true);
        meleeUpgrades.SetActive(false);
        rangedUpgrades.SetActive(false);
        characterUpgrades.SetActive(false);
    }

    public void OpenMeleeShop()
    {
        shopHome.SetActive(false);
        meleeUpgrades.SetActive(true);
    }

    public void OpenRangedShop()
    {
        shopHome.SetActive(false);
        rangedUpgrades.SetActive(true);
    }

    public void OpenCharacterShop()
    {
        shopHome.SetActive(false);
        characterUpgrades.SetActive(true);
    }

    public void CloseShopScreen()
    {
        shopInitial.SetActive(false);
        shopHome.SetActive(false);
        meleeUpgrades.SetActive(false);
        rangedUpgrades.SetActive(false);
        characterUpgrades.SetActive(false);
        CloseDescription();
    }

    public void CloseDescription()
    {
        descriptionBox.gameObject.SetActive(false);
    }
}
