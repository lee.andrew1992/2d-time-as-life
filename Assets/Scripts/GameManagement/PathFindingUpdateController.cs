﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFindingUpdateController : MonoBehaviour
{
    public bool needToUpdate;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdatePathFinding", 0, 1f);
    }

    void UpdatePathFinding()
    {
        if (needToUpdate)
        {
            GameManager.Instance.ResetPathFindingGraph();
            needToUpdate = false;
        }
    }

    public void RequestUpdate()
    {
        if (!needToUpdate)
        {
            needToUpdate = true;
        }
    }
}
