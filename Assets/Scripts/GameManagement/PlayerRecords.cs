﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using TMPro;

public class PlayerRecords : MonoBehaviour
{
    public List<string> recordStrings;
    public TextMeshProUGUI recordText;

    private int recordIndex = -1;

    // Start is called before the first frame update
    void Start()
    {
        recordStrings.Add(string.Format(StringValues.RecordsString.CurrentHighScore, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.HighScore, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.GameBeatenCount, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesGameBeaten, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.HighestCombo, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.HighestCombo, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.DeathCount, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesDied, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.HitCount, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesHit, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.UpgradesBoughtCount, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.UpgradeBoughtCount, 0)));
        recordStrings.Add(string.Format(StringValues.RecordsString.FastestRound, (int)PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.FastestRound, 40)));
        recordStrings.Add(string.Format(StringValues.RecordsString.EnemiesKilled, PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.EnemiesKilled, 0)));

        InvokeRepeating("UpdateRecordShown", 0, 4);
    }

    void UpdateRecordShown()
    {
        recordIndex++;

        if (recordIndex == recordStrings.Count)
        {
            recordIndex = 0;
        }

        recordText.text = recordStrings[recordIndex];
    }
}
