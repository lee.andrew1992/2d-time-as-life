﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using ConstantValues;

public class PointsSystem : MonoBehaviour
{
    public TextMeshProUGUI pointsCounter;
    public TextMeshProUGUI pointsMultiplier;

    private long points;
    private int currentMultiplier;

    public int CurrentMultiplier { get { return currentMultiplier; } }
    public long CurrentPoints { get { return points; } }

    // Start is called before the first frame update
    void Start()
    {
        points = 0;
        currentMultiplier = 1;

        UpdatePointsCounter();
    }

    void Update()
    {
        CalculateMultiplier();
        pointsMultiplier.gameObject.SetActive(currentMultiplier > 1);
        
        if (currentMultiplier > 1)
        { 
            pointsMultiplier.text = String.Format(StringValues.PointsSystem.ComboMultiplier, currentMultiplier);
        }
    }

    public void AddPoints(int basePoints)
    {
        points += basePoints * currentMultiplier;
        UpdatePointsCounter();
    }

    private void UpdatePointsCounter()
    {
        pointsCounter.text = String.Format("{0:n0}", points);
    }

    private void CalculateMultiplier()
    {
        currentMultiplier = GameManager.Instance.comboSystem.CurrentCombo < 10 ? 1 : (int)(GameManager.Instance.comboSystem.CurrentCombo / 10);
    }
}
