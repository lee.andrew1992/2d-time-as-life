﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ConstantValues;

public class TimerLogic : MonoBehaviour
{
    public TextMeshProUGUI gameTimer;
    public TextMeshProUGUI roundTimer;
    public TextMeshProUGUI roundDelayTimer;
    public TextMeshProUGUI buyTimer;

    public TMP_FontAsset penaltyFont;
    public TMP_FontAsset bonusFont;
    TMP_FontAsset gameTimerOriginalFont;

    public SoundEffect suddenDeathAlert;

    private float gameTime;
    private float roundTime;
    private float roundDelayTime;
    private float buyTime;

    private const float GAMETIME_PENALTY_EFFECT_DURATION = 0.3f;
    private const float GAMETIME_BONUS_EFFECT_DURATION = 0.3f;

    void Awake()
    {
        gameTimerOriginalFont = gameTimer.font;
        suddenDeathAlert = new SoundEffect(suddenDeathAlert.source);
    }

    void Update()
    {
        gameTimer.text = TranslateTime(gameTime);
        roundTimer.text = TranslateTime(roundTime);
        roundDelayTimer.text = TranslateTime(roundDelayTime);
        buyTimer.text = TranslateTime(buyTime);

        if (GameManager.Instance.roundStarted)
        {
            gameTime = Mathf.Max(gameTime - Time.deltaTime, 0);
            roundTime = Mathf.Max(roundTime - Time.deltaTime, 0);

            if (gameTime <= 0)
            {
                if (roundTime > 0)
                {
                    if (!GameManager.Instance.suddenDeath)
                    { 
                        suddenDeathAlert.Play();
                        GameManager.Instance.suddenDeath = true;
                    }
                }
                else
                {
                    GameManager.Instance.gameStartLogic.EndGame(true);
                }
            }
            else
            {
                if (GameManager.Instance.suddenDeath )
                {
                    suddenDeathAlert.Stop();
                    GameManager.Instance.suddenDeath = false;
                }
            }
        }
        else
        {
            suddenDeathAlert.Stop();

            if (roundDelayTimer.gameObject.activeSelf)
            { 
                roundDelayTime -= Time.deltaTime;
            }

            if (buyTimer.gameObject.activeSelf)
            {
                buyTime -= Time.deltaTime;
            }
        }
    }

    public float GetGameTime()
    {
        return gameTime;
    }

    public void SetRoundTime(float roundTime)
    {
        this.roundTime = roundTime;
        roundTimer.gameObject.SetActive(true);
    }

    public void SetRoundDelayTime(float roundDelayTime)
    {
        this.roundDelayTime = roundDelayTime;
        roundDelayTimer.gameObject.SetActive(true);
    }

    public void SetGameTime(float gameTime)
    {
        this.gameTime = gameTime;
        gameTimer.gameObject.SetActive(true);
    }

    public void SetBuyTime(float buyTime)
    {
        this.buyTime = buyTime;
        buyTimer.gameObject.SetActive(true);
    }

    public void HideRoundDelayTimer()
    {
        HideTimer(roundDelayTimer);
    }

    public void HideRoundTimer()
    {
        HideTimer(roundTimer);
    }

    public void HideBuyTimer()
    {
        HideTimer(buyTimer);
    }

    private void HideTimer(TextMeshProUGUI timer)
    {
        timer.gameObject.SetActive(false);
    }

    public void AddRoundTimeToGameTime(float startRoundTime)
    {
        float fastestRound = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.FastestRound, startRoundTime);

        float timePassed = startRoundTime - roundTime;
        if (timePassed < fastestRound)
        {
            PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.FastestRound, timePassed);
        }

        gameTime = Mathf.Max(gameTime, 0);
        SetGameTime(gameTime + roundTime);
    }

    public void AddTimeToGameTime(float bonusTime)
    {
        if (bonusTime < 0)
        {
            StartCoroutine(GameTimePenaltyEffect());
        }
        else
        {
            StartCoroutine(GameTimeBonusEffect());
        }

        float newTime = Mathf.Max(gameTime + bonusTime, 0);
        SetGameTime(newTime);
    }

    private string TranslateTime(float timeValue)
    {
        timeValue += 0.5f;
        if (timeValue > 60)
        {
            return string.Format(StringValues.Timer.MinSecFormat, (int)(timeValue / 60), (int)(timeValue % 60));   
        }
        return string.Format(StringValues.Timer.SecFormat, (int)timeValue);
    }

    IEnumerator GameTimePenaltyEffect()
    { 
        float currPenaltyDuration = 0;
        gameTimer.font = penaltyFont;

        StartCoroutine(CommonFunctions.Shake(gameTimer.rectTransform, GAMETIME_PENALTY_EFFECT_DURATION, 0.02f, gameTimer.rectTransform.anchoredPosition));

        while (currPenaltyDuration < GAMETIME_PENALTY_EFFECT_DURATION)
        {
            currPenaltyDuration += Time.deltaTime;

            yield return null;
        }

        gameTimer.font = gameTimerOriginalFont;
    }

    IEnumerator GameTimeBonusEffect()
    {
        float currBonusDuration = 0;
        gameTimer.font = bonusFont;

        StartCoroutine(CommonFunctions.ScalePulse(gameTimer.transform, GAMETIME_BONUS_EFFECT_DURATION, 0.25f, gameTimer.transform.localScale));

        while (currBonusDuration < GAMETIME_PENALTY_EFFECT_DURATION)
        {
            currBonusDuration += Time.deltaTime;

            yield return null;
        }

        gameTimer.font = gameTimerOriginalFont;
    }
}
