﻿using System.Collections;
using TMPro;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class GameManager: MonoBehaviour {
    public int currentRound;
    public CharacterEntity_Player player;
    public bool pcDemoMode;

    [HeaderAttribute("Path Finding")]
    public AstarPath pathFinding;
    public PathFindingUpdateController pathFindingUpdateController;

    [HeaderAttribute("Game Values")]
    public float initialGameTime;
    public bool roundStarted;
    public bool suddenDeath;
    public TimerLogic timeLogic;
    public Transform playerBullets;
    public Transform enemyBullets;
    public Transform enemyBombs;
    public ScreenShake cameraShaker;

    [HeaderAttribute("Round Data")]
    public float roundTime;
    public float bonusRoundTime;
    public int bonusRoundTimeRound;
    public float roundDelayTime;
    public float buyTime;
    public int upgradeLevelIncreaseRound;

    [HeaderAttribute("Map Data")]
    public Collider2D mapArea;
    public MapController mapController;
    public ObstacleController obstacleController;
    public CreateWalls wallCreator;
    public CreateTraps trapCreator;

    [HeaderAttribute("Time Pick Up Spawn Logic")]
    public Transform timePickUps;
    public TimePickUpSpawnLogic timePickUpSpawnLogic;

    [HeaderAttribute("Gameplay GUI Elements")]
    public GameObject controls;
    public GameObject roundCount;
    public GameObject roundAlert;
    public GameStart gameStartLogic;
    public ResponsiveGUIManager responsiveGUI;

    [HeaderAttribute("Combo System")]
    public ComboSystem comboSystem;

    [HeaderAttribute("Points System")]
    public PointsSystem pointsSystem;

    [HeaderAttribute("Enemy Spawn Logic")]
    public EnemySpawner enemySpawner;

    [HeaderAttribute("Player Records")]
    public PlayerRecords playerRecords;
    public int thisPlayThroughEnemyKilledCount = 0;
    public int thisPlayThroughHitCount = 0;
    public int thisPlayThroughUpgradeCount = 0;

    [HeaderAttribute("Upgrades")]
    public Upgrades_Melee meleeUpgrades;
    public Upgrades_Ranged rangedUpgrades;
    public Upgrades_Ship shipUpgrades;

    private static GameManager instance;
    public static GameManager Instance {
        get { return instance; }
    }

    public List<CharacterEntity_Enemy> allEnemies;
    public List<TimePickUpLogic> allTimePickUps;

    void Awake()
    {
        allEnemies = new List<CharacterEntity_Enemy>();
        allTimePickUps = new List<TimePickUpLogic>();
        pcDemoMode = Application.isEditor;

        if (!pcDemoMode)
        { 
        }

        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            roundStarted = false;
        }
    }

    public void SetTimePickUp(TimePickUpLogic timePickUpLogic)
    {
        allTimePickUps.Add(timePickUpLogic);
    }

    public void RemoveTimePickUp(TimePickUpLogic timePickUpLogic)
    {
        allTimePickUps.Remove(timePickUpLogic);
    }

    public void SetEnemy(CharacterEntity_Enemy enemyEntity)
    {
        allEnemies.Add(enemyEntity);
    }

    public void RemoveEnemy(CharacterEntity_Enemy enemyEntity)
    {
        if (allEnemies.Contains(enemyEntity))
        { 
            allEnemies.Remove(enemyEntity);
        }
    }

    public void ShakeCam(float duration, float magnitudeMax, float magnitudeMin)
    {
        cameraShaker.ShakeCam(duration, magnitudeMax, magnitudeMin);
    }

    public GameObject[] GetEnemies()
    {
        GameObject[] enemies = new GameObject[allEnemies.Count];
        for (int index = 0; index < allEnemies.Count; index++)
        {
            enemies[index] = allEnemies[index].gameObject;
        }

        return enemies;
    }

    public void ResetPathFindingGraph()
    {
        pathFinding.Scan();
    }
}