﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoundsData
{
    public EnemySpawnValues[] rounds;
}
