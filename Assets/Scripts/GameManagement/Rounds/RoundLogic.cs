﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Newtonsoft.Json;
using ConstantValues;

public class RoundLogic : MonoBehaviour
{
    public TextAsset roundDetails;
    public EnemySpawner spawner;
    public TimerLogic timerLogic;
    public GameObject controlGui;
    public GameObject pointsCounter;
    public ShopMenu shopMenu;
    public GameObject maxLevelUpText;

    private RoundsData allRounds;
    private PlayerMovement playerMovement;

    private bool buyPhase;
    private float buyPhaseEnd;
    private float bonusRoundTime = 0;
    private float thisRoundTime = 0;

    void Start()
    {
        allRounds = JsonConvert.DeserializeObject<RoundsData>(roundDetails.text);
        playerMovement = GameManager.Instance.player.GetComponent<PlayerMovement>();
    }

    public IEnumerator StartRound()
    {
        HideRoundCount();
        timerLogic.SetRoundDelayTime(GameManager.Instance.roundDelayTime);
        yield return new WaitForSeconds(GameManager.Instance.roundDelayTime);
        timerLogic.HideRoundDelayTimer();
        UpdateRoundCount();

        EnemySpawnValues values = allRounds.rounds[GameManager.Instance.currentRound - 1];
        spawner.InitializeRound(values);

        string alertString = "";

        GameManager.Instance.roundStarted = true;
        if (GameManager.Instance.currentRound >= GameManager.Instance.bonusRoundTimeRound && 
            GameManager.Instance.currentRound % GameManager.Instance.bonusRoundTimeRound == 0)
        {
            bonusRoundTime += GameManager.Instance.bonusRoundTime;
            alertString += string.Format(StringValues.Round.Alert.AdditionalRoundTimeBonus, GameManager.Instance.bonusRoundTime);
        }

        if (GameManager.Instance.currentRound >= 10 && 
            GameManager.Instance.currentRound % 10 == 0)
        {
            alertString += "\n";
            alertString += StringValues.Round.Alert.LevelUp;
        }

        if (!string.IsNullOrEmpty(alertString))
        {
            SetRoundAlert(alertString);
        }

        thisRoundTime = GameManager.Instance.roundTime + bonusRoundTime;
        timerLogic.SetRoundTime(thisRoundTime);
        
        GameManager.Instance.obstacleController.ObstacleCreatedThisRound = false;
    }

    void Update()
    { 
        if (buyPhase && Time.time >= buyPhaseEnd)
        {
            EndBuyPhase();
            StartCoroutine(StartRound());
        }

        if (GameManager.Instance.pcDemoMode && Input.GetKeyDown(KeyCode.K))
        {
            StartBuyPhase();
        }
    }

    public void StartGame()
    {
        controlGui.SetActive(true);
        pointsCounter.SetActive(true);
        timerLogic.SetGameTime(GameManager.Instance.initialGameTime);
        StartCoroutine(StartRound());
    }

    public void EndRound()
    {
        GameManager.Instance.roundStarted = false;

        foreach (Transform bullet in GameManager.Instance.enemyBullets)
        {
            bullet.GetComponent<ProjectileLogic>().DeflectBullet(Vector3.zero, NumericalValues.Layers.Enemy, 0, 0);
        }

        foreach (MapCube cube in GameManager.Instance.obstacleController.wallCubes)
        {
            cube.TransitionToBackground(0);
        }

        timerLogic.HideRoundTimer();
        timerLogic.AddRoundTimeToGameTime(thisRoundTime);
        playerMovement.ResetDashCooldown();

        if (GameManager.Instance.currentRound == allRounds.rounds.Length)
        {
            GameManager.Instance.gameStartLogic.EndGame(false);
        }
        else
        {
            if (GameManager.Instance.currentRound <= 8 &&
                GameManager.Instance.currentRound >= GameManager.Instance.upgradeLevelIncreaseRound &&
                GameManager.Instance.currentRound % GameManager.Instance.upgradeLevelIncreaseRound == 0)
            {
                StartCoroutine(ShowMaxLevelUpAlert());

                foreach (Upgrades_Base.UpgradeFactor factor in GameManager.Instance.meleeUpgrades.upgradeFactors)
                {
                    factor.IncreaseMaxLevel();
                }

                foreach (Upgrades_Base.UpgradeFactor factor in GameManager.Instance.rangedUpgrades.upgradeFactors)
                {
                    factor.IncreaseMaxLevel();
                }

                foreach (Upgrades_Base.UpgradeFactor factor in GameManager.Instance.shipUpgrades.upgradeFactors)
                {
                    factor.IncreaseMaxLevel();
                }
            }

            StartBuyPhase();
        }
    }

    private void StartBuyPhase()
    {
        buyPhase = true;
        buyPhaseEnd = Time.time + GameManager.Instance.buyTime;
        timerLogic.SetBuyTime(GameManager.Instance.buyTime);
        GameManager.Instance.controls.SetActive(false);
        shopMenu.OpenShopScreen();
    }

    private void EndBuyPhase()
    {
        buyPhase = false;
        GameManager.Instance.controls.SetActive(true);
        shopMenu.CloseShopScreen();
        timerLogic.HideBuyTimer();
    }

    public void SkipBuyPhase()
    {
        buyPhaseEnd = Time.time;
    }

    private void UpdateRoundCount()
    {
        GameManager.Instance.currentRound++;
        GameObject roundCount = GameManager.Instance.roundCount;

        if (GameManager.Instance.comboSystem.CurrentCombo > 0)
        { 
            GameManager.Instance.comboSystem.ResetTimer();
        }
        roundCount.SetActive(true);
        roundCount.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = string.Format(StringValues.Round.Count, GameManager.Instance.currentRound);
    }

    private void HideRoundCount()
    {
        GameManager.Instance.roundCount.SetActive(false);
    }

    private void SetRoundAlert(string alertString)
    {
        GameObject roundAlert = GameManager.Instance.roundAlert;
        StartCoroutine(CommonFunctions.TempActive(roundAlert, 5));
        roundAlert.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = alertString;
    }

    private IEnumerator ShowMaxLevelUpAlert()
    {
        maxLevelUpText.SetActive(true);

        yield return new WaitForSeconds(5f);

        maxLevelUpText.SetActive(false);
    }
}
