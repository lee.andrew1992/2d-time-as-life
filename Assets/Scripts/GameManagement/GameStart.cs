﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using ConstantValues;
using TMPro;

public class GameStart : MonoBehaviour
{
    public RoundLogic roundLogic;
    public GameObject startGameGui;
    public GameObject gameOverScreen;
    public GameObject cornerCheesePreventer;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI highestComboText;
    public TextMeshProUGUI currentRoundText;
    public TextMeshProUGUI currentScore;

    [HeaderAttribute("Tutorial")]
    public GameObject tutorialBackground;
    public GameObject tutorialLabel;
    public GameObject tutorialStartButton;
    public GameObject tutorialProceedButton;
    public GameObject[] tutorialScreens;
    int tutorialIndex = -1;

    [HeaderAttribute("Credits")]
    public GameObject creditStartButton;
    public GameObject creditBackground;
    public GameObject creditProceedButton;
    public GameObject creditLabel;
    public GameObject[] creditScreens;
    int creditIndex = -1;

    public void StartGame()
    {
        roundLogic.StartGame();
        tutorialStartButton.SetActive(false);
        startGameGui.SetActive(false);
        creditStartButton.SetActive(false);
        cornerCheesePreventer.SetActive(true);
    }

    public void StartTutorial()
    {
        tutorialBackground.SetActive(true);
        startGameGui.SetActive(false);
        creditStartButton.SetActive(false);
        tutorialProceedButton.SetActive(true);
        tutorialStartButton.SetActive(false);
        tutorialLabel.SetActive(true);
        GameManager.Instance.player.gameObject.SetActive(false);
        ProceedTutorial();
    }

    public void StartCredits()
    { 
        creditBackground.SetActive(true);
        startGameGui.SetActive(false);
        tutorialStartButton.SetActive(false);
        creditStartButton.SetActive(false);
        creditLabel.SetActive(true);
        creditProceedButton.SetActive(true);

        GameManager.Instance.player.gameObject.SetActive(false);
        ProceedCredit();
    }

    public void EndGame(bool death)
    {
        foreach (CharacterEntity enemy in GameManager.Instance.allEnemies)
        {
            enemy.alive = false;
        }

        long currentPoints = GameManager.Instance.pointsSystem.CurrentPoints;

        GameManager.Instance.roundStarted = false;
        GameManager.Instance.roundCount.SetActive(false);
        GameManager.Instance.controls.SetActive(false);
        GameManager.Instance.pointsSystem.pointsCounter.transform.parent.gameObject.SetActive(false);
        GameManager.Instance.roundStarted = false;
        gameOverScreen.SetActive(true);

        float enemiesKilled = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.EnemiesKilled, 0);
        PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.EnemiesKilled, enemiesKilled + GameManager.Instance.thisPlayThroughEnemyKilledCount);

        float hitCount = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesHit, 0);
        PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.TimesHit, hitCount + GameManager.Instance.thisPlayThroughHitCount);

        float upgradeCount = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.UpgradeBoughtCount, 0);
        PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.UpgradeBoughtCount, upgradeCount + GameManager.Instance.thisPlayThroughUpgradeCount);

        if (death)
        {
            float timesDied = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesDied, 0) + 1;
            PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.TimesDied, timesDied);

            gameOverText.text = StringValues.GameOverScreen.GameOverDeath;
            currentRoundText.text = string.Format(StringValues.GameOverScreen.LastRound, GameManager.Instance.currentRound);
        }
        else
        {
            float timesBeaten = PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.TimesGameBeaten, 0) + 1;
            PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.TimesGameBeaten, timesBeaten);

            gameOverText.text = StringValues.GameOverScreen.GameOverFinish;
            currentRoundText.text = string.Format(StringValues.GameOverScreen.AllRoundsBeat, GameManager.Instance.currentRound);
        }
        currentScore.text = string.Format(StringValues.GameOverScreen.CurrentScore, currentPoints);

        if (PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.HighScore, 0) < currentPoints)
        {
            PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.HighScore, currentPoints);
            currentScore.text = StringValues.PointsSystem.NewHighScoreTag + currentScore.text;
        }

        highestComboText.text = string.Format(StringValues.GameOverScreen.HighestCombo, GameManager.Instance.comboSystem.HighestCombo);

        if (PlayerPrefs.GetFloat(StringValues.SavedRecordsKey.HighestCombo, 0) < GameManager.Instance.comboSystem.HighestCombo)
        {
            PlayerPrefs.SetFloat(StringValues.SavedRecordsKey.HighestCombo, GameManager.Instance.comboSystem.HighestCombo);
            highestComboText.text = StringValues.PointsSystem.NewHighComboTag + highestComboText.text;
        }
    }

    public void ProceedCredit()
    {
        if (creditIndex >= 0)
        {
            creditScreens[creditIndex].SetActive(false);
        }

        if (creditIndex == creditScreens.Length - 1)
        {
            TutorialCreditEnd();
        }
        else
        {
            creditIndex++;
            creditScreens[creditIndex].SetActive(true);
        }
    }

    public void ProceedTutorial()
    {
        if (tutorialIndex >= 0)
        {
            tutorialScreens[tutorialIndex].SetActive(false);
        }

        if (tutorialIndex == tutorialScreens.Length - 1)
        {
            TutorialCreditEnd();
        }
        else
        { 
            tutorialIndex++;
            tutorialScreens[tutorialIndex].SetActive(true);
        }
    }

    public void ResetGame()
    {
        foreach (MapCube cube in GameManager.Instance.obstacleController.wallCubes)
        {
            cube.TransitionToBackground(0);
        }

        gameOverScreen.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void TutorialCreditEnd()
    {
        tutorialIndex = -1;
        creditIndex = -1;

        startGameGui.SetActive(true);
        
        tutorialStartButton.SetActive(true);
        creditStartButton.SetActive(true);
        
        tutorialBackground.SetActive(false);
        creditBackground.SetActive(false);
        
        tutorialLabel.SetActive(false);
        creditLabel.SetActive(false);

        tutorialProceedButton.SetActive(false);
        creditProceedButton.SetActive(false);
        
        GameManager.Instance.player.gameObject.SetActive(true);
    }
}
