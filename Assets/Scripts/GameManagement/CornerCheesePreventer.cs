﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CornerCheesePreventer : CreateTraps
{
    public float penaltyThreshhold;
    float stayDuration;

    void Start()
    {
        affectedMapCubes = new List<MapCube>();

        foreach (Transform child in this.transform)
        {
            GetCubesInArea(child.GetComponent<Collider2D>());
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (GameManager.Instance.roundStarted && collider.gameObject.layer == NumericalValues.Layers.Player)
        {
            stayDuration += Time.deltaTime;

            if (stayDuration > penaltyThreshhold)
            {
                CreateCornerPreventer();
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.layer == NumericalValues.Layers.Player)
        {
            Reset();
        }
    }

    void CreateCornerPreventer()
    {
        Reset();
        DoTransition(MapCube.TransitionTo.Trap);
    }

    public void Reset()
    {
        stayDuration = 0;
    }
}
