﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SoundEffect
{
    public AudioSource source;
    public float originalPitch;
    public float originalVolume;

    public SoundEffect(AudioSource effect)
    {
        source = effect;
        originalPitch = effect.pitch;
        originalVolume = effect.volume;
    }

    public void Play(float pitchMultiplier)
    {
        source.pitch = originalPitch * pitchMultiplier;
        Play();
    }
    public void Play()
    {
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }
}