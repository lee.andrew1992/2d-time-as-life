﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using TMPro;

public class TitleColorChanger : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public float minimumValue;
    public float colorSpeed;

    public float glowOffsetMin;
    public float glowOffsetMax;
    public float glowSpeed;

    Vector3 rgb = Vector3.right;
    Color currentColor;

    Color red;
    Color blue;
    Color green;

    float glowOffset;
    bool glowOffsetIncrease = true;

    void Start()
    { 
        currentColor = titleText.fontMaterial.GetColor(ShaderUtilities.ID_GlowColor);
        glowOffset = titleText.fontMaterial.GetFloat(ShaderUtilities.ID_GlowOffset);

        red = new Color(1, minimumValue, minimumValue);
        green = new Color(minimumValue, 1, minimumValue);
        blue = new Color(minimumValue, minimumValue, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (rgb == Vector3.right)
        {
            currentColor = Color.Lerp(currentColor, red, Time.deltaTime * colorSpeed);
            
            if (currentColor.r > 0.95f)
            {
                currentColor.r = 1;
                rgb = Vector3.up;
            }
        }
        else if (rgb == Vector3.up)
        {
            currentColor = Color.Lerp(currentColor, blue, Time.deltaTime * colorSpeed);
            if (currentColor.b > 0.95f)
            {
                currentColor.b = 1;
                rgb = Vector3.forward;
            }
        }
        else
        {
            currentColor = Color.Lerp(currentColor, green, Time.deltaTime * colorSpeed);

            if (currentColor.g > 0.95f)
            {
                currentColor.g = 1;
                rgb = Vector3.right;
            }
        }


        if (glowOffsetIncrease)
        {
            glowOffset = Mathf.Min(glowOffset + Time.deltaTime * glowSpeed, glowOffsetMax);
        }
        else
        {
            glowOffset = Mathf.Max(glowOffset - Time.deltaTime * glowSpeed, glowOffsetMin);
        }

        if (glowOffset == glowOffsetMin)
        {
            glowOffsetIncrease = true;
        }
        if (glowOffset == glowOffsetMax)
        {
            glowOffsetIncrease = false;
        }

        titleText.fontMaterial.SetColor(ShaderUtilities.ID_GlowColor, currentColor);
        titleText.fontMaterial.SetColor("_SpecularColor", currentColor);
        titleText.fontMaterial.SetFloat(ShaderUtilities.ID_GlowOffset, glowOffset);
    }
}
