﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FontSizer : MonoBehaviour
{
    public TextMeshProUGUI [] allTexts;

    private const float SCREEN_WIDTH_MAX = 1920;
    private const float SCREEN_HEIGHT_MAX = 1080;

    private Vector2 currentScreenSize;

    private float widthRatio;
    private float heightRatio;

    // Start is called before the first frame update
    void Start()
    {
        allTexts = Resources.FindObjectsOfTypeAll<TextMeshProUGUI>();
        currentScreenSize = CommonFunctions.RecordScreenSizeChange();
        ResizeText();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentScreenSize.x != Screen.width || currentScreenSize.y != Screen.height)
        {
            ResizeText();
            currentScreenSize = CommonFunctions.RecordScreenSizeChange();
        }
    }

    private void ResizeText()
    {
        widthRatio = Screen.width / SCREEN_WIDTH_MAX;
        heightRatio = Screen.height / SCREEN_HEIGHT_MAX;

        float currScreenRatio = (widthRatio + heightRatio) / 2;

        foreach (TextMeshProUGUI text in allTexts)
        {
            text.fontSize = text.fontSize * currScreenRatio;
        }
    }
}
