﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponsiveGUIManager : MonoBehaviour
{
    public GameObject playerHitAlert;
    public GameObject enemyPointsAlert;
    public GameObject timePickUpAlert;

    public enum AlertType
    { 
        PlayerHit,
        EnemyPoints,
        TimePickUp
    }

    public GameObject CreateUI(AlertType alertType, Vector2 basePosition, Vector2 offset)
    {
        GameObject alert;
        switch (alertType)
        { 
            case AlertType.EnemyPoints:
                alert = GameObject.Instantiate(this.enemyPointsAlert, GameManager.Instance.responsiveGUI.transform);
                break;

            case AlertType.PlayerHit:
                alert = GameObject.Instantiate(this.playerHitAlert, GameManager.Instance.responsiveGUI.transform);
                break;

            case AlertType.TimePickUp:
                alert = GameObject.Instantiate(this.timePickUpAlert, GameManager.Instance.responsiveGUI.transform);
                break;

            default:
                alert = new GameObject();
                break;
        }

        alert.transform.position = basePosition + offset;
        alert.SetActive(true);

        return alert;
    }
}
