﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GUIPlacer : MonoBehaviour
{
    public bool equalProportion;
    public bool resizeChildren;
    public bool reactiveGUI;

    public Vector2 size;
    public Vector2 location;

    Vector2 currentScreenSize;

    RectTransform rectTransform;
    
    void Awake()
    {
        rectTransform = this.GetComponent<RectTransform>();
        
        ResizeUI();
        RelocateUI();
        currentScreenSize = CommonFunctions.RecordScreenSizeChange();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentScreenSize.x != Screen.width || currentScreenSize.y != Screen.height)
        {
            ResizeUI();
            RelocateUI();
            currentScreenSize = CommonFunctions.RecordScreenSizeChange();
        }
    }

    void ResizeUI()
    {
        if (equalProportion)
        {
            rectTransform.sizeDelta = new Vector2(size.x * Screen.width, size.x * Screen.width);
        }
        else
        {
            rectTransform.sizeDelta = new Vector2 (size.x * Screen.width, size.y * Screen.height);
        }

        if (resizeChildren)
        {
            ResizeChildren(this.transform);
        }
    }

    void RelocateUI()
    {
        if (!reactiveGUI)
        { 
            rectTransform.anchoredPosition = new Vector3(Screen.width * location.x, Screen.height * location.y, 0);
        }
    }

    void ResizeChildren(Transform parent)
    {
        foreach (Transform child in parent)
        {
            child.GetComponent<RectTransform>().sizeDelta = rectTransform.sizeDelta;
            ResizeChildren(child);
        }
    }
}
