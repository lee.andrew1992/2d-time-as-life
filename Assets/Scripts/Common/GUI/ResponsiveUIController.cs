﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResponsiveUIController : MonoBehaviour
{
    TextMeshProUGUI thisText;
    public float textDuration;
    public float moveSpeed;
    private float alphaReductionSpeed;

    void Awake()
    {
        thisText = this.GetComponent<TextMeshProUGUI>();
        alphaReductionSpeed = thisText.alpha / textDuration;
    }

    // Update is called once per frame
    void Update()
    {
        thisText.alpha -= Time.deltaTime * alphaReductionSpeed;

        if (thisText.alpha <= 0)
        {
            Destroy(this.gameObject);
        }

        thisText.transform.position += Vector3.up * Time.deltaTime * moveSpeed;
    }

    public void SetText(string newText)
    {
        thisText.text = newText;
    }
}
