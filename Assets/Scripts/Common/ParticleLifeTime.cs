﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLifeTime : MonoBehaviour
{
    public float dissolveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CommonFunctions.Dissolve(this.GetComponent<ParticleSystemRenderer>().material, dissolveSpeed, this.gameObject));   
    }
}
