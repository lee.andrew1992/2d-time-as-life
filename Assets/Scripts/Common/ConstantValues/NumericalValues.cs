﻿using System.Collections;
using System.Collections.Generic;

namespace ConstantValues
{
    public static class NumericalValues
    {
        public static class Layers
        {
            public const int Player = 8;
            public const int Enemy = 9;
            public const int PlayerBullet = 10;
            public const int OuterWall = 11;
            public const int EnemyBullet = 12;
            public const int Invincible = 13;
            public const int WallTurner = 14;
            public const int TimePickUp = 15;
            public const int BackGround = 16;
            public const int ObstacleWall = 17;
            public const int TrapWall = 18;
            public const int PlayerCloseRangeAttack = 19;
        }

        public static class RenderLayer
        {
            public const int Bullets = 99;
            public const int CloseRangeAttack = 99;
            public const int ImpactParticleEffects = 99;
            public const int PlayerModel = 2;
            public const int TimePickUp = 2;
            public const int PlayerBlackOutline = 1;
            public const int Enemies = 2;
        }

        public static class RenderDistanceMaxMultiplier
        {
            public const int EnemyHitImpact = 2;
            public const int WallHitImpact = 3;
            public const int Projectile = 2;
        }

        public static class EnemyValues
        {
            public const int MaxLevel = 10;

            public static class Kamikaze
            {
                public const int Damage = 8888;
            }
        }

        public static class Projectile
        {
            public const float DefaultLifeTime = 5f;
            public const int DefaultBonusDeflectionAngleMax = 30;
        }

        public static class UpgradeCost
        { 
            public const int Minimum = 10;
        }

        public static class GameEnvironment
        { 
            public const int FrameRate = 30;
            public const float FixedTimeStepDefault = 0.0334f;
        }
    }

}
