﻿using System.Collections;
using System.Collections.Generic;

namespace ConstantValues {
    public static class StringValues 
    {
        public static class Player
        {
            public static class Input
            {
                public const string Horizontal = "Horizontal";
                public const string Vertical = "Vertical";
            }
        }

        public static class AnimationParam
        {
            public static class Common
            {
                public const string Facing_Up = "Facing_Up";
                public const string Facing_Down = "Facing_Down";
                public const string Facing_Horizontal = "Facing_Horizontal";
                public const string Moving = "Moving";
                public const string Attack = "Attack";
                public const string Bleed = "Bleed{0}";
            }
        }

        public static class ShaderVariable
        {
            public static class Common
            {
                public const string MainColor = "_MainColor";
            }

            public static class Dissolve
            {
                public const string Fade = "_Fade";
                public const string FadeScale = "_FadeScale";
                public const string FresnelValue = "_FresnelValue";
                public const string FadeColor = "_FadeColor";
            }

            public static class AlphaCutOff
            {
                public const string AlphaCutOffValue = "_AlphaCutOff";
            }
        }

        public static class Timer
        {
            public const string MinSecFormat = "{0}m:{1}s";
            public const string SecFormat = "{0}s";
        }

        public static class SavedRecordsKey
        { 
            public const string HighScore = "HighScore";
            public const string TimesGameBeaten = "TimesGameBeaten";
            public const string TimesDied = "TimesDied";
            public const string TimesHit = "TimesHit";
            public const string UpgradeBoughtCount = "UpgradesBoughtCount";
            public const string FastestRound = "FastestRound";
            public const string EnemiesKilled = "EnemiesKilled";
            public const string HighestCombo = "HighestCombo";
        }

        public static class RecordsString
        { 
            public const string CurrentHighScore = "Current High Score:  <font=stentiga SDF Text><color=green>{0:n0}</color></font>";
            public const string GameBeatenCount = "Total Game Beaten Count:  <font=stentiga SDF Text><color=green>{0:n0}</color></font>";
            public const string DeathCount = "Total Death Count:  <font=stentiga SDF Text><color=red>{0:n0}</color></font>";
            public const string HitCount = "Total HitS Taken Count:  <font=stentiga SDF Text><color=red>{0:n0}</color></font>";
            public const string UpgradesBoughtCount = "Total Upgrade Count:  <font=stentiga SDF Text><color=green>{0:n0}</color></font>";
            public const string FastestRound = "Fastest Round:  <font=stentiga SDF Text><color=green>{0}s</color></font>";
            public const string EnemiesKilled = "Total Enemies Killed:  <font=stentiga SDF Text><color=green>{0:n0}</color></font>";
            public const string HighestCombo = "Highest Combo:  <font=stentiga SDF Text><color=green>{0:n0}</color></font>";
        }

        public static class PointsSystem
        {
            public const string ComboMultiplier = "X{0}";
            public const string NewHighScoreTag = "<color=yellow>New High Score!</color>\n";
            public const string NewHighComboTag = "<color=yellow>New Highest Combo!</color>\n";
        }

        public static class Tags
        {
            public const string Music = "Music";
            public const string Map = "Map";
        }

        public static class Round
        {
            public const string Count = "Round {0}";

            public static class Alert
            {
                public const string AdditionalRoundTimeBonus = "Round Time Increased by <color=green>{0}s</color>";
                public const string LevelUp = "All Enemies have Leveled Up by <color=red>1</color>";
            }
        }

        public static class GameOverScreen
        {
            public const string CurrentScore = "Score:\n<color=green>{0:n0}</color>";
            public const string HighestCombo = "Highest Combo:\n<color=green>{0}</color>";
            public const string GameOverDeath = "<b><color=red>Game Over</color></b>\n\nTouch Screen to Restart Game";
            public const string GameOverFinish = "<b><color=green>Congratulations!</color></b>\n\nTouch Screen to Restart Game";
            public const string AllRoundsBeat = "<color=green>Finished All {0} Rounds!</color>";
            public const string LastRound = "Current Round:\n<color=green>{0}</color>";
        }

        public static class UpgradeDescriptions
        {
            public static class Common
            {
                public const string Level = "Current Level:\n{0} / {1}";
                public const string MaxLevel = "Current Level:\n<color=green>{0} / {1}</color>";
                public const string CostValue = "{0}m:{1}s";
                public const string CostValueSeconds = "{0}s";
                public const string Cost = "Cost:\n{0}% - <color=red>{1}</color>";
                public const string CostPreview = "\n{0} - <color=red>{1}</color> = <color=green>{2}</color> Left";
                public const string MinCost = "\n<color=red>Minimum Cost: {0}s</color>";
            }
            public static class Melee
            {
                public const string Range = "Description:\nIncrease Attack Range by <color=green>{0}%</color>\nMax Level: {1}";
                public const string AttackRate = "Description:\nIncrease Attack Rate by <color=green>{0}%</color>\nMax Level: {1}";
                public const string AttackAngle = "Description:\nIncrease Effective Angle by <color=green>{0}%</color>\nMax Level: {1}";
                public const string DeflectionBonus = "Description:\nIncrease additional projectiles when Deflecting Projectiles by <color=green>{0}</color>\nMax Level: {1}";
                public const string DamageIncrease = "Description:\nIncrease Damage by <color=green>{0}x</color>\nMax Level: {1}";
            }

            public static class Ranged
            {
                public const string BulletCount = "Description:\nIncrease Projectile Count by <color=green>{0}</color>\nMax Level: {1}";
                public const string BulletSpeed = "Description:\nIncrease Projectile Speed by <color=green>{0}%</color>\nMax Level: {1}";
                public const string FireRate = "Description:\nIncrease Projectile Fire Rate by <color=green>{0}%</color>\nMax Level: {1}";
                public const string MoveSpeedReduction = "Description:\nDecrease Move Speed Reduction by <color=green>{0}%</color>\nMax Level: {1}";
                public const string ProjectilePierce = "Description:\nProjectiles will now <color=green>Pierce Enemies</color>\nMax Level: {0}";
            }

            public static class Ship
            {
                public const string DashSpeed = "Description:\nIncrease Dash Speed by <color=green>33%</color>\nMax Level: {0}";
                public const string DashCooldown = "Description:\nReduce Dash Cooldown by <color=green>{0}s</color>\nMax Level: {1}";
                public const string InvincibilityDuration = "Description:\nIncrease On-Hit Invincibility Duration by <color=green>{0}s</color>\nMax Level: {1}";
            }
        }
    }
}
