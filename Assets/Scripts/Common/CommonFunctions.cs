﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using System.Linq;
using TMPro;

public static class CommonFunctions
{
    public static IEnumerator Dissolve(Material dissolveMat, float dissolveSpeed, GameObject obj = null)
    {
        while (dissolveMat.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) > 0)
        {
            dissolveMat.SetFloat(StringValues.ShaderVariable.Dissolve.Fade,
                dissolveMat.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) - (Time.deltaTime * dissolveSpeed));
            yield return null;
        }

        if (obj != null )
            GameObject.Destroy(obj);
    }

    public static void Shuffle<T>(T[] array)
    {
        System.Random random = new System.Random(); ;
        int index = array.Length;
        while (index > 1) 
        {
            int newIndex = random.Next(index--);
            T temp = array[index];
            array[index] = array[newIndex];
            array[newIndex] = temp;
        }
    }

    public static bool WithinTwoValues(float value, float min, float max)
    {
        return value > min && value < max;
    }

    public static Queue<T> ArrayToQueue<T>(T[] array)
    {
        Queue<T> resultQueue = new Queue<T>();

        foreach (T element in array)
        {
            resultQueue.Enqueue(element);
        }

        return resultQueue;
    }

    public static int PositiveOrNegative()
    {
        return (Random.Range(0, 2) * 2 - 1);
    }

    public static ParticleSystem CreateParticleSystem(ParticleSystem effect, Color color, Transform parent = null)
    {
        ParticleSystem newEffect = GameObject.Instantiate(effect);
        if (parent != null)
        { 
            newEffect.transform.SetParent(parent);
            newEffect.transform.localPosition = Vector3.zero;
        }

        ParticleSystemRenderer effectRenderer = newEffect.GetComponent<ParticleSystemRenderer>();
        effectRenderer.material.SetColor(StringValues.ShaderVariable.Common.MainColor, color);

        return newEffect;
    }

    public static Vector2 RecordScreenSizeChange()
    {
        return new Vector2(Screen.width, Screen.height);
    }

    public static float CalculateRandomAngle(Vector2 originalDir, int minAngle, int maxAngle, int sign)
    {
        int randomAngle = Random.Range(minAngle, maxAngle);

        float radians = 0;
        if (sign > 0)
        {
            radians = (Vector2.SignedAngle(Vector2.right, originalDir) + randomAngle) * Mathf.Deg2Rad;
        }
        else if (sign < 0)
        {
            radians = (Vector2.SignedAngle(Vector2.right, originalDir) - randomAngle) * Mathf.Deg2Rad;
        }

        return radians;
    }

    public static bool OutsideCamera(Camera cam, Entity entity, Vector2 camOffset)
    {
        return OutsideCamera(cam, entity.transform.position, camOffset);
    }

    public static bool OutsideCamera(Camera cam, Vector3 position, Vector2 camOffset)
    {
        Vector3 camMin = cam.ViewportToWorldPoint(new Vector3(0, 0, cam.transform.position.z));
        Vector3 camMax = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.transform.position.z));

        return !(CommonFunctions.WithinTwoValues(position.x, camMin.x - camOffset.x, camMax.x + camOffset.x) &&
            CommonFunctions.WithinTwoValues(position.y, camMin.y - camOffset.y, camMax.y + camOffset.y));
    }

    public static List<T> SortListByDistance<T>(Vector2 originPosition, List<T> list) where T: MonoBehaviour
    {
        return list.OrderBy(
            x => Vector2.Distance(originPosition, x.transform.position)
            ).ToList();
    }

    public static IEnumerator Shake(Transform objectTransform, float duration, float shakeVariation, Vector2 originalPosition)
    {
        float currDurationTimer = 0;
        while (currDurationTimer < duration)
        {
            objectTransform.position = originalPosition * Random.Range(1 - shakeVariation, 1 + shakeVariation);
            currDurationTimer += Time.deltaTime;
            yield return null;
        }
        objectTransform.position = originalPosition;
    }

    public static IEnumerator Shake(RectTransform guiTransform, float duration, float shakeVariation, Vector2 originalAnchoredPosition)
    {
        float currDurationTimer = 0;
        while (currDurationTimer < duration)
        {
            guiTransform.anchoredPosition = originalAnchoredPosition * Random.Range(1 - shakeVariation, 1 + shakeVariation);
            currDurationTimer += Time.deltaTime;
            yield return null;
        }

        guiTransform.anchoredPosition = originalAnchoredPosition;
    }

    public static void SetTMPColor(TextMeshProUGUI text, Color color)
    {
        text.color = color;
        text.fontMaterial.SetColor(ShaderUtilities.ID_FaceColor, color);
        text.fontMaterial.SetColor(ShaderUtilities.ID_GlowColor, color);
        text.fontMaterial.SetColor("_SpecularColor", color);
    }

    public static IEnumerator ScalePulse(Transform objectTransform, float duration, float scaleVariation, Vector2 originalScale)
    {
        float currScaleDurationTimer = 0;
        while (currScaleDurationTimer < duration)
        {
            float scaleDirection = 1;
            if (currScaleDurationTimer < duration / 2)
            {
                scaleDirection = (1 + scaleVariation);
            }
            else
            { 
                scaleDirection = (1 - scaleVariation);
            }
            
            objectTransform.localScale = Vector3.Lerp(objectTransform.localScale, originalScale * scaleDirection, Time.deltaTime * 2);
            currScaleDurationTimer += Time.deltaTime;

            yield return null;
        }

        objectTransform.localScale = originalScale;
    }

    public static bool ObjectInColliderArea(GameObject gObj, Collider2D collider)
    {
        return collider.OverlapPoint(gObj.transform.position);
    }

    public static void DisableCube(MapCube cube, float delay, out bool obstacleHit)
    {
        obstacleHit = false;
        if (cube.gameObject.layer == NumericalValues.Layers.ObstacleWall || cube.transitioning || cube.gameObject.layer == NumericalValues.Layers.TrapWall)
        {
            obstacleHit = true;
            cube.StopAllCoroutines();
            cube.ChangeToBackground();
        }

        cube.Disable(delay);
    }

    public static List<GameObject> TurnToGameObjectList<T>(List<T> listToTurn) where T: MonoBehaviour
    {
        List<GameObject> gameObjectList = new List<GameObject>();
        foreach (T element in listToTurn)
        { 
            gameObjectList.Add(element.gameObject);
        }

        return gameObjectList;
    }

    public static IEnumerator TempActive(GameObject gObj, float duration)
    {
        gObj.SetActive(true);
        yield return new WaitForSeconds(duration);

        gObj.SetActive(false);
    }
}
