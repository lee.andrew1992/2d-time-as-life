﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUISoundController : MonoBehaviour
{
    public SoundEffect pushSoundEffect;

    void Start()
    {
        pushSoundEffect = new SoundEffect(pushSoundEffect.source);
    }

    public void PlaySoundEffect()
    {
        pushSoundEffect.Play();
    }
}
