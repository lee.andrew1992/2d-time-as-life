﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController_Music : SoundController
{
    public AudioClip[] backGroundMusicList;

    private AudioSource audioSource;
    private float defaultVolume;
    private int currentBgmIndex;

    bool fadingOut = false;

    void Start()
    {
        Instantiate();
    }

    protected override void Instantiate()
    {
        base.Instantiate();
        
        audioSource = GetComponent<AudioSource>();
        defaultVolume = audioSource.volume;
        CommonFunctions.Shuffle(backGroundMusicList);
        currentBgmIndex = 0;
        audioSource.clip = backGroundMusicList[currentBgmIndex];
        StartBackgroundMusic();
    }

    void StartBackgroundMusic()
    {
        StartCoroutine(FadeIn(SoundGroup.Master, 0));
        audioSource.Play();
    }

    void Update()
    {
        if (!audioSource.isPlaying)
        {
            fadingOut = false;
            currentBgmIndex++;

            if (currentBgmIndex >= backGroundMusicList.Length)
            {
                currentBgmIndex = 0;
            }

            audioSource.clip = backGroundMusicList[currentBgmIndex];
            StartCoroutine(FadeIn(SoundGroup.Music, 0.85f));
            audioSource.Play();
        }
        else
        {
            float currPlayTimeLeft = audioSource.clip.length - audioSource.time;
            if (currPlayTimeLeft < 2 && !fadingOut)
            {
                fadingOut = true;
                StartCoroutine(FadeOut(SoundGroup.Music, 0.85f));
            }
        }
    }
}
