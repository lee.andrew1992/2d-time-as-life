﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using ConstantValues;
using System;

public class SoundController : MonoBehaviour
{
    public AudioMixer mixer;
    private const float MIXER_VOLUME_MIN = -80;
    private const float MIXER_VOLUME_MAX = 0;

    public enum SoundGroup
    { 
        Master = 0,
        Effect = 1,
        Music = 2,
        DeathEffect = 3
    }

    public enum SoundGroupVariable
    { 
        Volume = 0,
        Pitch = 1
    }

    public List<float> defaultVolumes;

    public static SoundController instance;

    void Start()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
            Instantiate();
        }


        foreach (Transform child in this.transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public IEnumerator FadeIn(SoundGroup group, float startVolumeMultiplier)
    {
        float startVolume;
        string volumeVar = group.ToString() + SoundGroupVariable.Volume.ToString();

        mixer.GetFloat(volumeVar, out startVolume);

        if (startVolume >= MIXER_VOLUME_MIN)
        {
            float volumeRatio = 1 - startVolumeMultiplier;

            float currVolume = MIXER_VOLUME_MIN * volumeRatio;
            mixer.SetFloat(volumeVar, currVolume);


            while (currVolume < defaultVolumes[(int)group])
            {
                currVolume += Time.deltaTime * volumeRatio * 20f;
                mixer.SetFloat(volumeVar, currVolume);
                yield return null;
            }

            mixer.SetFloat(volumeVar, defaultVolumes[(int)group]);
        }
    }

    public IEnumerator FadeOut(SoundGroup group, float targetVolumeMultiplier)
    {
        float startVolume;
        string volumeVar = group.ToString() + SoundGroupVariable.Volume.ToString();
        mixer.GetFloat(volumeVar, out startVolume);

        if (startVolume > MIXER_VOLUME_MIN)
        {
            float diff = Mathf.Abs(startVolume) - Mathf.Abs(MIXER_VOLUME_MIN);
            float currVolume = startVolume;

            float volumeRatio = 1 - Mathf.Abs(startVolume / MIXER_VOLUME_MIN);

            while (currVolume > MIXER_VOLUME_MIN * (1 - targetVolumeMultiplier))
            {
                currVolume -= Time.deltaTime * volumeRatio * 20f;
                mixer.SetFloat(volumeVar, currVolume);
                yield return null;
            }

            mixer.SetFloat(volumeVar, MIXER_VOLUME_MIN * (1 - targetVolumeMultiplier));
        }
    }

    protected virtual void Instantiate()
    {
        defaultVolumes = new List<float>();

        foreach (SoundGroup group in Enum.GetValues(typeof(SoundGroup)))
        {
            string volumeVar = group.ToString() + SoundGroupVariable.Volume.ToString();
            float defaultVolume = 0;
            mixer.GetFloat(volumeVar, out defaultVolume);
            defaultVolumes.Insert((int)group, defaultVolume);
        }
    }

    public IEnumerator ChangePitch(SoundGroup group, float newPitch, float duration)
    {
        string pitchVar = group.ToString() + SoundGroupVariable.Pitch.ToString();
        mixer.SetFloat(pitchVar, newPitch);

        float currDuration = 0;
        while(currDuration < duration)
        {
            currDuration += Time.deltaTime;
            yield return null;
        }

        mixer.SetFloat(pitchVar, 1);
    }
}
