﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class ProjectileCreator : MonoBehaviour
{
    [ColorUsage(true, true)]
    public Color bulletColor;

    GameObject projectile;
    int damage;
    float speed;
    Transform parent;
    int targetLayer;

    public void Instantiate(GameObject projectilePrefab, int projectileDamage, float projectileSpeed, Transform projectileParent, int enemyLayer)
    {
        projectile = projectilePrefab;
        damage = projectileDamage;
        speed = projectileSpeed;
        parent = projectileParent;
        targetLayer = enemyLayer;
    }

    public void CreateProjectileAttack(Vector2 originalDir, int projectileCount, int angleGap, int pierce = 0)
    {
        if (projectileCount > 1)
        {
            CreateBullets(originalDir, projectileCount, angleGap, pierce);
        }
        else
        {
            CreateBullet(originalDir, pierce);
        }
    }

    private void CreateBullet(Vector2 attackDirection, int pierce)
    {
        CreateBullet(attackDirection, attackDirection, pierce);
    }

    private void CreateBullet(Vector2 projectileDirection, Vector2 attackDirection, int pierce)
    {
        Vector2 basePosition = new Vector2(this.transform.position.x, this.transform.position.y) + (attackDirection);

        GameObject bullet = GameObject.Instantiate(projectile, parent);
        bullet.transform.position = basePosition;
        bullet.transform.GetChild(0).GetComponent<Renderer>().material.SetColor(StringValues.ShaderVariable.Common.MainColor, bulletColor);
        float zRot = Vector2.SignedAngle(Vector2.up, attackDirection);
        bullet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, zRot));

        bullet.GetComponent<ProjectileLogic>().Instantiate(projectileDirection, speed, damage, targetLayer, pierce);
    }

    private void CreateBullets(Vector2 originalDir, int projectileCount, int angleGap, int pierce)
    {
        for (int index = 0; index < projectileCount; index++)
        {
            int minAngle = (int)(index / 2) * (angleGap / 2);
            int maxAngle = minAngle + angleGap / 2;

            float radians;
            if (index % 2 == 0)
            {
                radians = CommonFunctions.CalculateRandomAngle(originalDir, minAngle, maxAngle, 1);
            }
            else
            {
                radians = CommonFunctions.CalculateRandomAngle(originalDir, minAngle, maxAngle, -1);
            }

            Vector2 newAngle = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));

            CreateBullet(newAngle, originalDir, pierce);
        }
    }
}
