﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class ProjectileLogic : MonoBehaviour
{
    private float speed;
    private int damage;
    private Vector3 direction;
    private bool instantiated = false;
    private int targetLayer;
    private int pierce = 0;

    private float lifeTime;

    private Renderer thisRenderer;

    public ParticleSystem wallHitImpact;

    public GameObject bulletModel;

    void Start()
    {
        thisRenderer = this.transform.GetChild(0).GetComponent<Renderer>();
        lifeTime = NumericalValues.Projectile.DefaultLifeTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (instantiated)
        {
            this.transform.position += direction * speed * Time.deltaTime;
            thisRenderer.enabled = !CommonFunctions.OutsideCamera(Camera.main, this.transform.position, Vector2.one * NumericalValues.RenderDistanceMaxMultiplier.Projectile);
            
            if (lifeTime > 0)
            {
                lifeTime -= Time.deltaTime;
            }

            else
            {
                KillBullet();
            }
        }
    }

    public void Instantiate(Vector3 projectileDirection, float projectileSpeed, int projectileDamage, int projectileTargetLayer, int projectilePierce)
    {
        direction = projectileDirection;
        speed = projectileSpeed;
        damage = projectileDamage;
        targetLayer = projectileTargetLayer;
        pierce = projectilePierce;
        SetBulletLayer();
        instantiated = true;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ContactPoint2D contactPoint = new ContactPoint2D();
        ParticleSystem impact = null;

        if (collision.collider.gameObject.layer == targetLayer)
        {
            if (collision.collider.gameObject.layer == NumericalValues.Layers.Player)
            {
                collision.collider.gameObject.GetComponent<CharacterEntity>().TakeDamage(damage, direction);
            }
            else
            {
                collision.collider.gameObject.GetComponent<CharacterEntity>().TakeDamage(damage, direction, bulletModel);
            }
            contactPoint = GetContactPoint(collision.contacts, targetLayer);

            if (pierce == 0)
            {
                KillBullet();
            }
        }

        if (collision.collider.gameObject.layer == ConstantValues.NumericalValues.Layers.OuterWall || collision.collider.gameObject.layer == ConstantValues.NumericalValues.Layers.ObstacleWall)
        {
            List<int> wallLayers = new List<int>() { ConstantValues.NumericalValues.Layers.OuterWall, ConstantValues.NumericalValues.Layers.ObstacleWall };
            contactPoint = GetContactPoint(collision.contacts, wallLayers);

            if (!CommonFunctions.OutsideCamera(Camera.main, this.transform.position, Vector2.one * NumericalValues.RenderDistanceMaxMultiplier.WallHitImpact))
            {
                impact = CommonFunctions.CreateParticleSystem(wallHitImpact, bulletModel.GetComponent<SpriteRenderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor));
                impact.transform.position = contactPoint.point;
                impact.transform.LookAt(impact.transform.position + Vector3.Reflect(direction, contactPoint.normal));
                //impact.transform.LookAt((Vector2)impact.transform.position + contactPoint.normal);
            }

            if (collision.collider.gameObject.layer == ConstantValues.NumericalValues.Layers.ObstacleWall)
            {
                collision.collider.gameObject.GetComponent<ObstacleEntity_Wall>().TakeDamage(damage, direction);
            }

            KillBullet();
        }
    }

    ContactPoint2D GetContactPoint(ContactPoint2D[] contactPoints, int layer)
    {
        return GetContactPoint(contactPoints, new List<int>() { layer });
    }

    ContactPoint2D GetContactPoint(ContactPoint2D [] contactPoints, List<int> layers)
    {
        foreach (ContactPoint2D contactPoint in contactPoints)
        {
            if (layers.Contains(contactPoint.collider.gameObject.layer))
            {
                return contactPoint;
            }
        }

        return new ContactPoint2D();
    }

    Vector3 CalculateImpactRotation(Vector2 normal)
    {
        if (normal == Vector2.up)
        {
            return new Vector3(270,0,0);
        }
        if (normal == Vector2.down)
        {
            return new Vector3(90, 0, 0);
        }
        if (normal == Vector2.right)
        {
            return new Vector3(0, 90, 0);
        }
        if (normal == Vector2.left)
        {
            return new Vector3(0, 270, 0);
        }

        return Vector3.zero;
    }

    public void DeflectBullet(Vector2 deflectionDirection, int newTargetLayer, int bonusDeflectionCount)
    {
        ResetLifeTime();
        targetLayer = newTargetLayer;
        SetBulletLayer();
        this.direction = deflectionDirection;

        float zRot = Vector2.SignedAngle(Vector2.up, deflectionDirection);
        this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, zRot));

        for (int index = 0; index < bonusDeflectionCount; index++)
        {
            GameObject duplicate = CreateDuplicate();

            float radians = CommonFunctions.CalculateRandomAngle(deflectionDirection, 0, NumericalValues.Projectile.DefaultBonusDeflectionAngleMax / 2, CommonFunctions.PositiveOrNegative());
            Vector2 newAngle = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));

            duplicate.GetComponent<ProjectileLogic>().DeflectBullet(newAngle, newTargetLayer, 0);
        }
    }

    public void DeflectBullet(Vector2 deflectionDirection, int newTargetLayer, int bonusDeflectionCount, float newSpeed)
    {
        if (newSpeed == 0)
        {
            KillBullet();
        }
        else
        { 
            this.speed = newSpeed;
            DeflectBullet(deflectionDirection, newTargetLayer, bonusDeflectionCount);
        }
    }

    public GameObject CreateDuplicate()
    {
        GameObject duplicate = GameObject.Instantiate(this.gameObject, this.transform.parent);
        duplicate.GetComponent<ProjectileLogic>().Instantiate(this.direction, this.speed, this.damage, this.targetLayer, this.pierce);

        return duplicate;
    }

    public void SetBulletLayer()
    {
        switch (targetLayer)
        {
            case NumericalValues.Layers.Enemy:
                this.gameObject.layer = NumericalValues.Layers.PlayerBullet;
                break;

            case NumericalValues.Layers.Player:
                this.gameObject.layer = NumericalValues.Layers.EnemyBullet;
                break;
        }
    }

    public int GetTargetLayer()
    {
        return targetLayer;
    }

    void ResetLifeTime()
    {
        lifeTime = NumericalValues.Projectile.DefaultLifeTime;        
    }

    public int GetDamage()
    {
        return damage;
    }

    public void KillBullet()
    {
        Destroy(this.gameObject);    
    }
}
