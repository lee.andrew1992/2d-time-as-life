﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackBase : MonoBehaviour
{
    public Joystick attackStick;
    
    public float damage;
    public float attacksPerMinute;
    public bool turnOffOther;
    public List<GameObject> otherControls;

    public Transform attackOrigin;

    [HeaderAttribute("Camera Shake Variables")]
    public float shakeDuration;
    public float shakeMagnitudeMax;
    public float shakeMagnitudeMin;

    protected int targetLayer;
    protected float nextAttack;

    [HeaderAttribute("Sound Effect")]
    public SoundEffect soundEffect;

    protected Vector2 AttackDirection()
    {
        if (!attackStick)
        {
            return Vector2.zero;
        }

        Vector2 attackDir = Vector2.zero;
        attackStick.enabled = attackStick.transform.parent.gameObject.activeSelf;

        if (attackStick.enabled)
        {
            attackDir = new Vector2(attackStick.Horizontal, attackStick.Vertical).normalized;
        }
        else
        {
            attackStick.Reset();
        }
        
        if (turnOffOther)
        {
            if (attackDir != Vector2.zero)
            {
                ToggleOthers(false);
            }
            else
            {
                ToggleOthers(true);
            }
        }

        return attackDir;
    }

    protected void DebugDrawAttackDirection(Color color)
    {
        Debug.DrawRay(this.transform.position, AttackDirection(), color);
    }

    private void ToggleOthers(bool state)
    {
        foreach (GameObject otherControl in otherControls)
        {
            otherControl.SetActive(state);
        }
    }
}
