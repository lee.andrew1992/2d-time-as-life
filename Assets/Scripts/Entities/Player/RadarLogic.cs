﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class RadarLogic : MonoBehaviour
{
    public GameObject indicatorPrefab;
    public Camera cam;

    // Update is called once per frame
    void Update()
    {
        List<CharacterEntity_Enemy> enemies = GameManager.Instance.allEnemies;
        CreateIndicators<CharacterEntity_Enemy>(enemies);

        List<TimePickUpLogic> timePickUps = GameManager.Instance.allTimePickUps;
        CreateIndicators<TimePickUpLogic>(timePickUps);
    }

    void SetIndicatorRotation(Entity entity)
    {
        Vector3 direction = GameManager.Instance.player.transform.position - entity.transform.position;

        float angle = Vector2.SignedAngle(Vector2.down, direction);
        entity.RadarIndicator.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    void RemoveIndicator(Entity entity)
    {
        GameObject.Destroy(entity.RadarIndicator);
        entity.RadarIndicator = null;
    }

    void SetIndicator(Entity entity)
    {
        GameObject indicator = GameObject.Instantiate(indicatorPrefab, this.transform);
        switch (entity.gameObject.layer)
        { 
            case NumericalValues.Layers.Enemy:
                indicator.GetComponent<SpriteRenderer>().material.SetColor(StringValues.ShaderVariable.Common.MainColor, 
                                                                           ((CharacterEntity)entity).coreModel.GetComponent<SpriteRenderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor));
                break;

            case NumericalValues.Layers.TimePickUp:
                indicator.GetComponent<SpriteRenderer>().material.SetColor(StringValues.ShaderVariable.Common.MainColor, 
                                                                           ((TimePickUpLogic)entity).model.GetComponent<SpriteRenderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor));
                break;
        }
        
        entity.RadarIndicator = indicator;
    }

    void CreateIndicators<T>(List<T> entities) where T : Entity
    {
        foreach (T entity in entities)
        {
            if (CommonFunctions.OutsideCamera(cam, entity, Vector2.zero))
            {
                if (!entity.RadarIndicator)
                {
                    SetIndicator(entity);
                }

                SetIndicatorRotation(entity);
            }
            else
            {
                if (entity.RadarIndicator)
                {
                    RemoveIndicator(entity);
                }
            }
        }
    }
}
