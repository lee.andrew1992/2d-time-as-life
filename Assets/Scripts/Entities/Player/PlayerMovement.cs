﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed;
    public float dashDuration;
    public float dashSpeedModifier;
    public float dashCooldown;
    public GameObject model;
    public RectTransform dashCooldownIndicatorCanvas;
    [ColorUsage(true, true)]
    public Color cooldownCompleteColor;
    [ColorUsage(true, true)]
    public Color cooldownIncompleteColor;

    public TrailRenderer trail;

    [ColorUsage(true, true)]
    public Color boostTrailColor;
    [ColorUsage(true, true)]
    public Color normalTrailColor;

    public Joystick joystick;

    private bool dashing = false;
    private Vector3 movementDir;
    private Rigidbody2D rb;
    private float currMoveSpeed;
    private float nextDashTime;
    public Image dashCooldownIndicator;
    public Image dashButtonCooldownIndicator;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currMoveSpeed = movementSpeed;
        nextDashTime = 0;
        trail.material.SetColor(StringValues.ShaderVariable.Common.MainColor, normalTrailColor);
    }

    void Update()
    {
        UpdateDashCooldownIndicator();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Dash();
        }

        float horizontalInput = 0;
        float verticalInput = 0;
        joystick.enabled = joystick.transform.parent.gameObject.activeSelf;

        if (joystick.enabled)
        {
            horizontalInput = joystick.Horizontal;
            verticalInput = joystick.Vertical;

            if (GameManager.Instance.pcDemoMode)
            {
                horizontalInput = Input.GetAxis("Horizontal");
                verticalInput = Input.GetAxis("Vertical");
                if (Input.GetAxisRaw("Dash") > 0)
                {
                    DoDash();
                }
            }
        }
        else
        {
            joystick.Reset();
        }

        movementDir = new Vector3(horizontalInput, verticalInput, 0).normalized;
        transform.Translate(movementDir * Time.deltaTime * currMoveSpeed);

        if (movementDir != Vector3.zero)
        { 
            float zRot = Vector2.SignedAngle(Vector2.up, movementDir);
            model.transform.rotation = Quaternion.Euler(new Vector3 (0,0,zRot));
        }
    }

    public void Dash()
    {
        if (!dashing && movementDir != Vector3.zero && Time.time > nextDashTime && 
            GameManager.Instance.player.gameObject.layer == NumericalValues.Layers.Player) // this is so that dash cannot be used while invincible
        {
            GameManager.Instance.comboSystem.ResetTimer(1.5f);
            StartCoroutine(DoDash());
        }
    }

    IEnumerator DoDash()
    {
        float currDashDuration = 0;
        float dashSpeed = currMoveSpeed * dashSpeedModifier;
        nextDashTime = Time.time + dashCooldown;
        trail.material.SetColor(StringValues.ShaderVariable.Common.MainColor, boostTrailColor);

        GameManager.Instance.player.TurnInvincible(dashDuration);

        while (currDashDuration < dashDuration)
        {
            dashing = true;
            currMoveSpeed = dashSpeed;
            currDashDuration += Time.deltaTime;
            yield return null;
        }

        trail.material.SetColor(StringValues.ShaderVariable.Common.MainColor, normalTrailColor);
        dashing = false;
        currMoveSpeed = movementSpeed;
    }

    public void SlowDown(float speedReduction)
    {
        currMoveSpeed = movementSpeed * speedReduction;
    }

    public void RestoreSpeed()
    {
        currMoveSpeed = movementSpeed;
    }

    public void ResetDashCooldown()
    {
        nextDashTime = 0;
    }

    void UpdateDashCooldownIndicator()
    {
        dashCooldownIndicatorCanvas.localPosition = Vector3.zero;
        dashCooldownIndicatorCanvas.sizeDelta = new Vector2(Screen.width, Screen.height);
        if (dashCooldownIndicator.fillAmount < 1)
        {
            dashCooldownIndicator.material.SetColor(StringValues.ShaderVariable.Common.MainColor, cooldownIncompleteColor);
            dashButtonCooldownIndicator.material.SetColor(StringValues.ShaderVariable.Common.MainColor, cooldownIncompleteColor);
        }
        else
        {
            dashCooldownIndicator.material.SetColor(StringValues.ShaderVariable.Common.MainColor, cooldownCompleteColor);
            dashButtonCooldownIndicator.material.SetColor(StringValues.ShaderVariable.Common.MainColor, cooldownCompleteColor);
        }

        if (nextDashTime == 0)
        {
            dashCooldownIndicator.fillAmount = 1;
            return;
        }

        float dashCooldownFillValue = Mathf.Min(1 - ((nextDashTime - Time.time) / dashCooldown), 1);
        dashCooldownIndicator.fillAmount = dashCooldownFillValue;
    }

    public Vector2 GetCurrentMovement(bool withSpeed = false)
    {
        return withSpeed ? movementDir * movementSpeed : movementDir;
    }
}
