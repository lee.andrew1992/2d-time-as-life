﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using ConstantValues;

public class ScreenShake : MonoBehaviour
{
    public float dampingSpeed;
    CinemachineBasicMultiChannelPerlin virtualCamNoise;

    private float shakeMagnitudeMin;
    private float currentShakeMagnitude;
    private float currentShakeDuration;

    void Start()
    {
        virtualCamNoise = this.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentShakeDuration > 0)
        {
            currentShakeMagnitude = Mathf.Max(currentShakeMagnitude - (Time.deltaTime * dampingSpeed), shakeMagnitudeMin);
            virtualCamNoise.m_AmplitudeGain = currentShakeMagnitude;
            currentShakeDuration -= Time.deltaTime * dampingSpeed;
        }

        else
        {
            virtualCamNoise.m_AmplitudeGain = 0;
            currentShakeDuration = 0;
        }
    }

    public void ShakeCam(float shakeDuration, float shakeMagnitudeMax, float shakeMagnitudeMin)
    {
        currentShakeDuration = shakeDuration;
        currentShakeMagnitude = shakeMagnitudeMax;
        this.shakeMagnitudeMin = shakeMagnitudeMin;
    }
}
