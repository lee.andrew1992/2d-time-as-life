﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class PlayerAttackBomb : PlayerAttackMelee
{
    private const int BOMB_ANGLE = 360;

    [HeaderAttribute("Bomb Specific")]
    [ColorUsage(true, true)]
    public Color bombMainColor;

    [ColorUsage(true, true)]
    public Color bombFadeColor;

    public float bombAttackDuration;
    public float bombSpreadSpeed;

    void Start()
    {
        soundEffect = new SoundEffect(soundEffect.source);
    }

    protected GameObject CreateBombEffect()
    {
        GameObject effect = GameObject.Instantiate(attackEffect);
        effect.GetComponent<BombAttackLogic>().DoAttack(bombAttackDuration, bombSpreadSpeed, (int)damage, bombMainColor, bombFadeColor);

        effect.transform.SetParent(attackOrigin);
        effect.transform.localPosition = Vector3.zero;

        return effect;
    }

    public void CreateAttack(bool mute = false)
    {
        if (!mute)
        { 
            soundEffect.Play(Random.Range(0.9f, 1.1f));
        }

        GameManager.Instance.ShakeCam(shakeDuration, shakeMagnitudeMax, shakeMagnitudeMin);
        Handheld.Vibrate();

        GameObject effect = CreateBombEffect();
    }
}
