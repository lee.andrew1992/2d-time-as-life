﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ConstantValues;

public class Upgrades_Melee : Upgrades_Base
{
    private PlayerAttackMelee meleeAttack;

    private enum MeleeUpgradeMap
    {
        Range = 0,
        AttackRate = 1,
        AttackAngle = 2,
        DeflectionBonus = 3,
        DamageIncrease = 4
    }

    void Start()
    {
        meleeAttack = GameManager.Instance.player.GetComponent<PlayerAttackMelee>();
    }

    public override void ShowUpgradeDetails(Button buyButton)
    {
        base.ShowUpgradeDetails(buyButton);
        UpgradeFactor currUpgrade = this.upgradeFactors[currentSelection];
        string costDescription = TranslateCost(currUpgrade);

        string description = "";

        switch (currentSelection)
        {
            case (int)MeleeUpgradeMap.Range:
                description = string.Format(StringValues.UpgradeDescriptions.Melee.Range, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)MeleeUpgradeMap.AttackRate:
                description = string.Format(StringValues.UpgradeDescriptions.Melee.AttackRate, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)MeleeUpgradeMap.AttackAngle:
                description = string.Format(StringValues.UpgradeDescriptions.Melee.AttackAngle, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)MeleeUpgradeMap.DeflectionBonus:
                description = string.Format(StringValues.UpgradeDescriptions.Melee.DeflectionBonus, currUpgrade.upgradeValue, currUpgrade.maxLevel);
                break;

            case (int)MeleeUpgradeMap.DamageIncrease:
                description = string.Format(StringValues.UpgradeDescriptions.Melee.DamageIncrease, currUpgrade.upgradeValue + 1, currUpgrade.maxLevel);
                break;
        }

        string levelDescription = TranslateLevel(currUpgrade);

        OpenDescription(description,
                        levelDescription,
                        costDescription);
    }

    public override void PurchaseUpgrade(Button button, int currentSelection)
    {
        switch (currentSelection)
        {
            case (int)MeleeUpgradeMap.Range:
                UpgradePlayer(ref meleeAttack.range, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)MeleeUpgradeMap.AttackRate:
                UpgradePlayer(ref meleeAttack.attacksPerMinute, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)MeleeUpgradeMap.AttackAngle:
                UpgradePlayer(ref meleeAttack.angle, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)MeleeUpgradeMap.DeflectionBonus:
                UpgradePlayer(ref meleeAttack.bonusDeflectionBulletCount, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)MeleeUpgradeMap.DamageIncrease:
                UpgradePlayer(ref meleeAttack.damage, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
        }
    }
}
