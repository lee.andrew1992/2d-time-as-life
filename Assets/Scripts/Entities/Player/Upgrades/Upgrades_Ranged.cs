﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ConstantValues;

public class Upgrades_Ranged : Upgrades_Base
{
    private PlayerAttackRanged rangedAttack;

    void Start()
    {
        rangedAttack = GameManager.Instance.player.GetComponent<PlayerAttackRanged>();
    }

    private enum RangedUpgradeMap
    { 
        BulletCount = 0,
        BulletSpeed = 1,
        FireRate = 2,
        SpeedReduction = 3,
        ProjectilePierce = 4
    }

    public override void ShowUpgradeDetails(Button buyButton)
    {
        base.ShowUpgradeDetails(buyButton);
        UpgradeFactor currUpgrade = this.upgradeFactors[currentSelection];
        string costDescription = TranslateCost(currUpgrade);

        string description = "";

        switch (currentSelection)
        { 
            case (int)RangedUpgradeMap.BulletCount:
                description = string.Format(StringValues.UpgradeDescriptions.Ranged.BulletCount, currUpgrade.upgradeValue, currUpgrade.maxLevel);
                break;

            case (int)RangedUpgradeMap.BulletSpeed:
                description = string.Format(StringValues.UpgradeDescriptions.Ranged.BulletSpeed, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)RangedUpgradeMap.FireRate:
                description = string.Format(StringValues.UpgradeDescriptions.Ranged.FireRate, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)RangedUpgradeMap.SpeedReduction:
                description = string.Format(StringValues.UpgradeDescriptions.Ranged.MoveSpeedReduction, currUpgrade.upgradeValue * 100, currUpgrade.maxLevel);
                break;

            case (int)RangedUpgradeMap.ProjectilePierce:
                description = string.Format(StringValues.UpgradeDescriptions.Ranged.ProjectilePierce, currUpgrade.maxLevel);
                break;
        }

        string levelDescription = TranslateLevel(currUpgrade);

        OpenDescription(description,
                        levelDescription,
                        costDescription);
    }

    public override void PurchaseUpgrade(Button button, int currentSelection)
    {
        switch (currentSelection)
        { 
            case (int)RangedUpgradeMap.BulletCount:
                UpgradePlayer(ref rangedAttack.projectileCount, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)RangedUpgradeMap.BulletSpeed:
                UpgradePlayer(ref rangedAttack.bulletSpeed, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)RangedUpgradeMap.FireRate:
                UpgradePlayer(ref rangedAttack.attacksPerMinute, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)RangedUpgradeMap.SpeedReduction:
                UpgradePlayer(ref rangedAttack.speedReduction, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)RangedUpgradeMap.ProjectilePierce:
                UpgradePlayer(ref rangedAttack.projectilePierce, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
        }
    }
}
