﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ConstantValues;

public class Upgrades_Ship : Upgrades_Base
{
    private PlayerMovement playerMovement;
    private CharacterEntity_Player playerEntity;

    private enum ShipUpgradeMap
    {
        DashSpeed = 0,
        DashCooldown = 1,
        InvincibilityDuration = 2
    }

    // Start is called before the first frame update
    void Start()
    {
        playerMovement = GameManager.Instance.player.GetComponent<PlayerMovement>();
        playerEntity = GameManager.Instance.player.GetComponent<CharacterEntity_Player>();
    }


    public override void ShowUpgradeDetails(Button buyButton)
    {
        base.ShowUpgradeDetails(buyButton);
        UpgradeFactor currUpgrade = this.upgradeFactors[currentSelection];
        string costDescription = TranslateCost(currUpgrade);

        string description = "";

        switch (currentSelection)
        {
            case (int)ShipUpgradeMap.DashSpeed:
                description = string.Format(StringValues.UpgradeDescriptions.Ship.DashSpeed, currUpgrade.maxLevel);
                break;

            case (int)ShipUpgradeMap.DashCooldown:
                description = string.Format(StringValues.UpgradeDescriptions.Ship.DashCooldown, Mathf.Abs(currUpgrade.upgradeValue), currUpgrade.maxLevel);
                break;
            case (int)ShipUpgradeMap.InvincibilityDuration:
                description = string.Format(StringValues.UpgradeDescriptions.Ship.InvincibilityDuration, currUpgrade.upgradeValue, currUpgrade.maxLevel);
                break;
        }

        string levelDescription = TranslateLevel(currUpgrade);

        OpenDescription(description,
                        levelDescription,
                        costDescription);
    }

    public override void PurchaseUpgrade(Button button, int currentSelection)
    {
        switch (currentSelection)
        {
            case (int)ShipUpgradeMap.DashSpeed:
                UpgradePlayer(ref playerMovement.dashSpeedModifier, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)ShipUpgradeMap.DashCooldown:
                UpgradePlayer(ref playerMovement.dashCooldown, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
            case (int)ShipUpgradeMap.InvincibilityDuration:
                UpgradePlayer(ref playerEntity.invincibilityTime, this.upgradeFactors[currentSelection]);
                ShowUpgradeDetails(button);
                break;
        }
    }
}
