﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using ConstantValues;
using TMPro;

public class Upgrades_Base : MonoBehaviour
{
    public GameObject descriptionParent;

    public TextMeshProUGUI descriptionSection;
    public TextMeshProUGUI costSection;
    public TextMeshProUGUI levelSection;

    public List<UpgradeFactor> upgradeFactors;
    protected int currentSelection;

    public enum UpgradeMethod
    { 
        Additive,
        Multiplicative
    }

    public enum CostType
    { 
        Multiplicative,
        Flat
    }

    [Serializable]
    public class UpgradeFactor
    {
        private const float COST_MAX = 0.7f;

        public string name;
        public int currentLevel;
        public int currentMaxLevel;
        public int maxLevel;
        public float cost;
        public float costIncrease;
        public float upgradeValue;
        public UpgradeMethod upgradeMethod;
        public CostType costType;

        public void IncreaseLevel()
        {
            cost += costIncrease;
            cost = Mathf.Min(cost, COST_MAX);
            currentLevel += 1;
        }

        public void IncreaseMaxLevel()
        {
            currentMaxLevel = Mathf.Min(currentMaxLevel+1, maxLevel);
        }
    }

    public void PayCost(UpgradeFactor factor)
    { 
        float currGameTime = GameManager.Instance.timeLogic.GetGameTime();

        float cost = 0;
        switch (factor.costType)
        { 
            case CostType.Multiplicative:
                cost = Mathf.Max(currGameTime * factor.cost, NumericalValues.UpgradeCost.Minimum);
                break;

            case CostType.Flat:
                cost = factor.cost;
                break;
        }

        GameManager.Instance.thisPlayThroughUpgradeCount++;

        GameManager.Instance.timeLogic.AddTimeToGameTime(-cost);
    }

    protected void OpenDescription(string description, string level, string cost)
    {
        descriptionParent.gameObject.SetActive(true);
        descriptionSection.text = description;
        levelSection.text = level;
        costSection.text = cost;
    }

    public void UpgradePlayer(ref float upgradeTarget, UpgradeFactor upgradeFactor)
    {
        if (GameManager.Instance.timeLogic.GetGameTime() <= NumericalValues.UpgradeCost.Minimum)
        {
            return;
        }

        switch (upgradeFactor.upgradeMethod)
        { 
            case UpgradeMethod.Additive:
                Debug.Log("Upgrading " + upgradeFactor.name + " | Level: " + upgradeFactor.currentLevel + " | Method: " + upgradeFactor.upgradeMethod + " | upgradeTarget: " + upgradeTarget);
                upgradeTarget += upgradeFactor.upgradeValue;
                break;

            case UpgradeMethod.Multiplicative:
                upgradeTarget = upgradeTarget * (1 + upgradeFactor.upgradeValue);
                break;
        }

        PayCost(upgradeFactor);
        upgradeFactor.IncreaseLevel();
    }

    public void CheckBuyButtonValid(Button buyButton, UpgradeFactor currUpgrade)
    {
        if (currUpgrade.currentLevel >= currUpgrade.currentMaxLevel || GameManager.Instance.timeLogic.GetGameTime() <= NumericalValues.UpgradeCost.Minimum)
        {
            buyButton.gameObject.SetActive(false);
        }
        else
        {
            buyButton.gameObject.SetActive(true);
        }
    }

    public virtual void SetCurrentUpgrade(int index)
    {
        currentSelection = index;
    }

    public virtual void ShowUpgradeDetails(Button buyButton)
    {
        UpgradeFactor currUpgrade = this.upgradeFactors[currentSelection];
        CheckBuyButtonValid(buyButton, currUpgrade);
    }

    protected string TranslateCost(UpgradeFactor currUpgrade)
    {
        string costDescription = "";
        if (currUpgrade.currentLevel == currUpgrade.maxLevel)
        {
            return costDescription;
        }

        int cost = Mathf.Max((int)(GameManager.Instance.timeLogic.GetGameTime() * currUpgrade.cost), NumericalValues.UpgradeCost.Minimum);

        string costValue = string.Format(StringValues.UpgradeDescriptions.Common.CostValueSeconds, cost);
        if (cost > 60)
        {
            int costMinutes = cost / 60;
            int costSeconds = cost - (costMinutes * 60);

            costValue = string.Format(StringValues.UpgradeDescriptions.Common.CostValue, costMinutes, costSeconds);
        }
        costDescription += string.Format(StringValues.UpgradeDescriptions.Common.Cost, currUpgrade.cost * 100, costValue);

        if (cost == NumericalValues.UpgradeCost.Minimum)
        {
            costDescription += string.Format(StringValues.UpgradeDescriptions.Common.MinCost, NumericalValues.UpgradeCost.Minimum);
        }

        return costDescription;
    }

    protected string TranslateLevel(UpgradeFactor currUpgrade)
    {
        string levelDescription = "";

        if (currUpgrade.currentLevel == currUpgrade.maxLevel)
        {
            levelDescription = string.Format(StringValues.UpgradeDescriptions.Common.MaxLevel, currUpgrade.currentLevel, currUpgrade.maxLevel);
        }
        else
        {
            levelDescription = string.Format(StringValues.UpgradeDescriptions.Common.Level, currUpgrade.currentLevel, currUpgrade.currentMaxLevel);
        }

        return levelDescription;
    }

    public virtual void PurchaseUpgrade(Button button, int currentSelection)
    {
    }

    public void SetBuyButton(Button buyButton)
    {
        buyButton.onClick.RemoveAllListeners();
        buyButton.onClick.AddListener(delegate { PurchaseUpgrade(buyButton, currentSelection); });
    }
}

