﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CloseRangeAttackEffect : MonoBehaviour
{
    public SpriteRenderer model;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private float RangeToScale(float range)
    {
        return range / 2.5f;
    }

    private IEnumerator StartAttackEffect(float range, float angle, float scaleSpeed)
    { 
        float maxScale = RangeToScale(range);
        float fadeScale = model.material.GetFloat(StringValues.ShaderVariable.Dissolve.FadeScale);
        // 180 degrees = 0.75f alpha cut off value
        model.material.SetFloat(StringValues.ShaderVariable.AlphaCutOff.AlphaCutOffValue, (angle / 180) * 0.75f);
        model.transform.GetChild(0).GetComponent<SpriteRenderer>().material.SetFloat(StringValues.ShaderVariable.AlphaCutOff.AlphaCutOffValue, (angle / 180) * 0.75f);

        while (this.transform.localScale.x < maxScale)
        {
            fadeScale += Time.deltaTime * scaleSpeed;
            model.material.SetFloat(StringValues.ShaderVariable.Dissolve.FadeScale, fadeScale);
            model.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, model.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) - Time.deltaTime * scaleSpeed / 4);
            this.transform.localScale += Vector3.one * Time.deltaTime * scaleSpeed;

            yield return null;
        }

        model.gameObject.SetActive(false);

        Destroy(this.gameObject, 0.25f);
    }

    public void StartAttack(float range, float angle, float scaleSpeed)
    {
        StartCoroutine(StartAttackEffect(range, angle, scaleSpeed));
    }
}
