﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CharacterEntity_Player : CharacterEntity
{
    public float invincibilityTime;
    public float hitPenalty;
    public float hitTimeSlowFactor;
    public float hitSlowMoDuration;

    private PlayerAttackBomb bomb;

    void Start()
    {
        bomb = this.GetComponent<PlayerAttackBomb>();
        this.alive = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void TakeDamage(int damage, Vector2 hitDirection)
    {
        bomb.CreateAttack();
        CommonFunctions.CreateParticleSystem(hitImpact, coreModel.GetComponent<SpriteRenderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor), this.transform);

        GameManager.Instance.thisPlayThroughHitCount++;

        if (GameManager.Instance.suddenDeath)
        {
            GameManager.Instance.gameStartLogic.EndGame(true);
        }
        else
        {
            GameObject playerHitAlert = GameManager.Instance.responsiveGUI.CreateUI(ResponsiveGUIManager.AlertType.PlayerHit, (Vector2)this.transform.position, Vector2.up * 0.3f);
            playerHitAlert.GetComponent<ResponsiveUIController>().SetText("-" + string.Format(StringValues.Timer.SecFormat, hitPenalty));

            GameManager.Instance.timeLogic.AddTimeToGameTime(-hitPenalty);
            GameManager.Instance.comboSystem.ResetCombo();
            TurnInvincible();
            StartCoroutine(HitSlowTime());
            StartCoroutine(SoundController.instance.FadeIn(SoundController.SoundGroup.Master, 0.8f));
        }
    }

    public void TurnInvincible()
    {
        StartCoroutine(DoInvincibility(invincibilityTime));
    }

    public void TurnInvincible(float duration)
    {
        StartCoroutine(DoInvincibility(duration));
    }

    private IEnumerator DoInvincibility(float duration)
    {
        float currInvincibility = 0;
        while (currInvincibility < duration)
        {
            this.gameObject.layer = NumericalValues.Layers.Invincible;
            currInvincibility += Time.deltaTime;
            coreModel.gameObject.SetActive(false);

            yield return null;
        }

        coreModel.gameObject.SetActive(true);
        this.gameObject.layer = NumericalValues.Layers.Player;
    }

    private IEnumerator HitSlowTime()
    {
        Time.timeScale = hitTimeSlowFactor;
        Time.fixedDeltaTime = hitTimeSlowFactor * NumericalValues.GameEnvironment.FixedTimeStepDefault;

        float currSlowMoDuration = 0;
        
        while (currSlowMoDuration < hitSlowMoDuration)
        {
            currSlowMoDuration += Time.unscaledDeltaTime;
            yield return null;
        }

        Time.fixedDeltaTime = NumericalValues.GameEnvironment.FixedTimeStepDefault;
        Time.timeScale = 1;
    }

    public override void Die()
    {
        base.Die();
        GameManager.Instance.gameStartLogic.EndGame(true);     
    }
}
