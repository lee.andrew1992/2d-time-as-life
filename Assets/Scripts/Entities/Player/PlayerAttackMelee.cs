﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class PlayerAttackMelee : PlayerAttackBase
{
    [HeaderAttribute("Melee Specific")]
    public float range;
    public float angle;
    public float baseDistanceValue;
    public GameObject attackEffect;

    [HeaderAttribute("Bullet Deflection")]
    public int bulletDeflectionAngle;
    public float deflectedBulletSpeed;
    public float bonusDeflectionBulletCount = 0;

    private uint currLayer;

    protected ProjectileCreator projectileCreator;

    void Start()
    {
        projectileCreator = GetComponent<ProjectileCreator>();
        soundEffect = new SoundEffect(soundEffect.source);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 attackDirection = AttackDirection();
        Debug.DrawRay(this.transform.position, attackDirection, Color.blue);

        if (attackDirection != Vector2.zero && nextAttack < Time.time)
        {
            nextAttack = Time.time + (60 / attacksPerMinute);
            StartCoroutine(CreateMeleeAttack(attackDirection));
            GameManager.Instance.ShakeCam(shakeDuration, shakeMagnitudeMax, shakeMagnitudeMin);
        }
    }

    private GameObject CreateMeleeEffect(Vector2 attackDirection, float scaleSpeed)
    {
        GameObject effect = GameObject.Instantiate(attackEffect, attackOrigin);
        effect.transform.localPosition = Vector3.zero;
        float zRot = Vector2.SignedAngle(Vector2.up, attackDirection);
        effect.transform.rotation = Quaternion.Euler(new Vector3(0, 0, zRot));
        effect.GetComponent<CloseRangeAttackEffect>().StartAttack(range, angle, scaleSpeed);
        
        currLayer++;

        return effect;
    }

    IEnumerator CreateMeleeAttack(Vector2 attackDirection)
    {
        soundEffect.Play(Random.Range(0.9f, 1.1f));

        float scaleSpeed = range * 1.5f;

        GameObject effect = CreateMeleeEffect(attackDirection, scaleSpeed);

        float currRange = 0;

        List<GameObject> enemiesAlreadyHit = new List<GameObject>();
        List<GameObject> wallsAlreadyHit = new List<GameObject>();

        while (effect != null)
        {
            currRange += Time.deltaTime * scaleSpeed * 2.5f;
            // Add 20% bonus to max range to make hit reg feel better at the end of attack effect
            currRange = Mathf.Min(currRange, range * 1.2f);

            GameObject[] enemiesInRange;
            if (CheckHit(attackDirection, out enemiesInRange, GameManager.Instance.GetEnemies(), currRange))
            {
                DealDamage(enemiesInRange, this.transform, effect, enemiesAlreadyHit);
            }

            GameObject[] enemyBulletsInRange;
            if (CheckHit(attackDirection, out enemyBulletsInRange, GameManager.Instance.enemyBullets, currRange))
            {
                DeflectBullets(enemyBulletsInRange, attackDirection);
            }

            List<GameObject> allWalls = CommonFunctions.TurnToGameObjectList<MapCube>(GameManager.Instance.obstacleController.wallCubes);
            GameObject[] wallsInRange;
            if (CheckHit(attackDirection, out wallsInRange, allWalls.ToArray(), currRange))
            {
                DealDamage(wallsInRange, this.transform, effect, wallsAlreadyHit);
            }
            yield return null;
        }
    }

    protected void DealDamage(GameObject[] enemiesInRange, Transform originTransform, GameObject effect, List<GameObject> alreadyHit)
    {
        foreach (GameObject enemy in enemiesInRange)
        {
            if (!alreadyHit.Contains(enemy))
            { 
                enemy.GetComponent<Entity>().TakeDamage((int)damage, enemy.transform.position - originTransform.position, effect);
                alreadyHit.Add(enemy);
            }
        }
    }

    protected void DeflectBullets(GameObject[] enemyBulletsInRange, Vector2 attackDirection)
    {
        foreach (GameObject bullet in enemyBulletsInRange)
        {
            Vector2 newAngle = Vector2.zero;

            if (deflectedBulletSpeed > 0)
            {
                bullet.transform.SetParent(GameManager.Instance.playerBullets);
                
                float radians;
                radians = CommonFunctions.CalculateRandomAngle(attackDirection, 0, bulletDeflectionAngle / 2, CommonFunctions.PositiveOrNegative());
                newAngle = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
            }
            
            bullet.GetComponent<ProjectileLogic>().DeflectBullet(newAngle, NumericalValues.Layers.Enemy, (int)bonusDeflectionBulletCount, deflectedBulletSpeed);
        }
    }

    protected virtual bool CheckHit(Vector2 attackDirection, out GameObject[] hits, Transform checkParent, float customRange = -1)
    {
        List<GameObject> objectList = new List<GameObject>();

        foreach (Transform child in checkParent)
        {
            objectList.Add(child.gameObject);
        }

        return CheckHit(attackDirection, out hits, objectList.ToArray(), customRange);
    }

    protected virtual bool CheckHit(Vector2 attackDirection, out GameObject[] hits, GameObject[] checkArray, float customRange = -1)
    {
        if (customRange < 0)
        {
            customRange = range;
        }

        hits = null;
        List<GameObject> objectList = new List<GameObject>();

        foreach (GameObject checkObject in checkArray)
        {
            if (Vector3.Distance(checkObject.transform.position, this.transform.position) < customRange)
            {
                Vector2 enemyDir = checkObject.transform.position - transform.position;
                if (Vector2.Angle(enemyDir, attackDirection) < angle / 2)
                {
                    objectList.Add(checkObject);
                }
            }
        }
        if (objectList.Count > 0)
        {
            hits = objectList.ToArray();
            return true;
        }

        return false;
    }

    void OnDrawGizmosSelected()
    {
        if (AttackDirection() == Vector2.zero)
            return;
        Vector2 attackRoot = AttackDirection() * baseDistanceValue;
        Vector2 attackEnd = AttackDirection() * range;
    }
}
