﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class PlayerAttackRanged : PlayerAttackBase
{
    [HeaderAttribute("Ranged Specific")]
    public GameObject projectile;

    public float bulletSpeed;
    public float projectileCount;
    public float angleSplitPerBulletMax;
    public float speedReduction;
    public float projectilePierce = 0;

    ProjectileCreator projectileCreator;
    PlayerMovement playerMovement;

    void Start()
    {
        nextAttack = 0;
        projectileCreator = this.GetComponent<ProjectileCreator>();
        projectileCreator.Instantiate(projectile, (int)damage, bulletSpeed, GameManager.Instance.playerBullets, NumericalValues.Layers.Enemy);
        playerMovement = this.GetComponent<PlayerMovement>();
        soundEffect = new SoundEffect(soundEffect.source);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 attackDirection = AttackDirection();

        if (GameManager.Instance.pcDemoMode)
        {
            attackDirection = new Vector2(Input.GetAxis("FireHorizontal"), Input.GetAxis("FireVertical")).normalized;
        }
            
        if (attackDirection != Vector2.zero)
        {
            playerMovement.SlowDown(speedReduction);

            if (nextAttack < Time.time)
            {
                CreateAttack(attackDirection);
                GameManager.Instance.ShakeCam(shakeDuration, shakeMagnitudeMax, shakeMagnitudeMin);
            }
        }
        else
        {
            playerMovement.RestoreSpeed();
        }
    }

    void CreateAttack(Vector2 attackDirection)
    {
        nextAttack = Time.time + (60 / attacksPerMinute);
        soundEffect.Play(Random.Range(0.9f, 1.1f));
        projectileCreator.CreateProjectileAttack(attackDirection, (int)projectileCount, (int)angleSplitPerBulletMax, (int)projectilePierce);
    }
}
