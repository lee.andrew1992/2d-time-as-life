﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class ObstacleEntity_Trap : ObstacleEntity_Base
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == NumericalValues.Layers.Player)
        {
            GameManager.Instance.player.TakeDamage(0, Vector2.zero);
        }
    }
}