﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleEntity_Wall : ObstacleEntity_Base
{
    public int maxHealth;
    private bool active;

    public void InstantiateObstacle()
    {
        health = maxHealth;
        active = true;
    }

    public override void TakeDamage(int damage, Vector2 hitDirection)
    {
        base.TakeDamage(damage, hitDirection);

        if (health < 0 && active)
        {
            active = false;
            this.GetComponent<MapCube>().TransitionToBackground(0);
        }
    }
}