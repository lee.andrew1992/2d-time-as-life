﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using UnityEngine.UI;

public class TimePickUpLogic : Entity
{
    public float bonusTimeValue;
    public float pickUpLifeTime;

    public GameObject model;
    public PlayerAttackBomb bombTrigger;

    public Image lifeIndicator;

    private float pickUpLifeTimeMax;

    void Start()
    {
        GameManager.Instance.SetTimePickUp(this);
        pickUpLifeTimeMax = pickUpLifeTime;
    }

    void Update()
    {
        if (GameManager.Instance.roundStarted)
        { 
            pickUpLifeTime -= Time.deltaTime;
            if (lifeIndicator.enabled)
            { 
                lifeIndicator.fillAmount = pickUpLifeTime / pickUpLifeTimeMax;
            }
        }

        if (pickUpLifeTime <= 0)
        {
            KillPickUp(0);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == NumericalValues.Layers.Player || collision.collider.gameObject.layer == NumericalValues.Layers.Invincible)
        {
            GameManager.Instance.timeLogic.AddTimeToGameTime(bonusTimeValue);

            GameObject timePickUpAlert = GameManager.Instance.responsiveGUI.CreateUI(ResponsiveGUIManager.AlertType.TimePickUp, this.transform.position, Vector2.up * 0.3f);
            timePickUpAlert.GetComponent<ResponsiveUIController>().SetText("+" + string.Format(StringValues.Timer.SecFormat, bonusTimeValue));

            bombTrigger.CreateAttack();
            StartCoroutine(KillPickUp(5));
        }
    }

    IEnumerator KillPickUp(float delay)
    {
        float currDelayTimer = 0;
        model.SetActive(false);
        lifeIndicator.enabled = false;
        this.GetComponent<Rigidbody2D>().simulated = false;
        this.GetComponent<Collider2D>().enabled = false;
        GameManager.Instance.RemoveTimePickUp(this);

        while (currDelayTimer < delay)
        {
            currDelayTimer += Time.deltaTime;
            yield return null;
        }

        GameObject.Destroy(this.gameObject);
    }
}
