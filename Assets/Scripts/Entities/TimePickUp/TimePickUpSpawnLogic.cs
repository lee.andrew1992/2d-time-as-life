﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePickUpSpawnLogic : MonoBehaviour
{
    public GameObject pickUpPrefab;
    public float pickUpSpawnInterval;
    public bool readyToSpawn;

    private float currSpawnInterval;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.roundStarted)
        {
            if (currSpawnInterval <= 0)
            {
                readyToSpawn = true;
            }
            else
            { 
                currSpawnInterval -= Time.deltaTime;
            }
        }
    }

    public void Spawn(Vector2 position)
    {
        Reset();
        GameObject timePickUp = GameObject.Instantiate(pickUpPrefab, GameManager.Instance.timePickUps);
        timePickUp.transform.position = position;
    }

    void Reset()
    {
        currSpawnInterval = pickUpSpawnInterval;
        readyToSpawn = false;
    }

    void SetRandomPosition(Transform newPickUp)
    {
        Vector2 areaMin = GameManager.Instance.mapArea.bounds.min;
        Vector2 areaMax = GameManager.Instance.mapArea.bounds.max;

        newPickUp.position = new Vector2(Random.Range(areaMin.x, areaMax.x), Random.Range(areaMin.y, areaMax.y));
    }
}
