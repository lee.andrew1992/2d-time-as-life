﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySpawnValues
{
    public int simultaneousEnemiesThisRound { get; set;}

    public EnemyDetails meleeNormal { get; set; }
    public EnemyDetails meleeTank { get; set; }
    public EnemyDetails rangedNormal { get; set; }
    public EnemyDetails rangedTank { get; set; }
    public EnemyDetails bombNormal { get; set; }

    public override string ToString()
    {
        return string.Format("simultaneousEnemiesThisRound: {0}, meleeNormal: {1}, meleeTank: {2}, rangedNormal: {3}, rangedTank: {4}, bombNormal: {5}", 
            simultaneousEnemiesThisRound, meleeNormal.ToString(), meleeTank.ToString(), rangedNormal.ToString(), rangedTank.ToString(), bombNormal.ToString());
    }


    public class EnemyDetails
    {
        public int count;
        public int level;

        public override string ToString()
        {
            return string.Format("count: {0}, level: {1}", count, level);
        }
    }
}