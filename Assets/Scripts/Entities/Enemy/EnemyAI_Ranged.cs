﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class EnemyAI_Ranged : EnemyAI_Base
{
    public GameObject projectile;
    public int projectileCount;
    public float bulletSpeed;
    public Transform bulletParent;
    public float shotInterval;
    public float timeRequiredToShoot;
    public int angleGap;

    ProjectileCreator projectileCreator;
    float nextShot;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate();
        projectileCreator = GetComponent<ProjectileCreator>();
        projectileCreator.Instantiate(projectile, attackDamage, bulletSpeed, GameManager.Instance.enemyBullets, NumericalValues.Layers.Player);

        nextShot = Time.time + shotInterval / 2;
    }

    void FixedUpdate()
    {
        AliveCheck();
        MoveToTargetAndAttack();
    }

    protected override void Instantiate()
    {
 	    base.Instantiate();
    }

    protected override IEnumerator Attack()
    {
        if (nextShot > Time.time)
        {
            yield return null;
        }
        else
        {
            Vector2 targetDirection = ((Vector2)target.position - (Vector2)rb.position).normalized;

            projectileCreator.CreateProjectileAttack(targetDirection, projectileCount, angleGap);
            nextShot = Time.time + shotInterval;
            
            float shotDuration = 0;
            while (shotDuration < timeRequiredToShoot)
            {
                shotDuration += Time.deltaTime;

                attacking = true;
                yield return null;
            }

            attacking = false;
        }
    }

    public override void SetLevel(int level)
    {
        this.level = Mathf.Min(level, NumericalValues.EnemyValues.MaxLevel);
        base.SetLevel(level);

        projectileCount += (int)((level - 1) / 2);
        bulletSpeed *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        speed *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        shotInterval *= 1f - ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        attackRange *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
    }
}
