﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private List<KeyValuePair<EnemyAI_Base, int>> allEnemies;
    private Queue<KeyValuePair<EnemyAI_Base, int>> enemySpawnQueue;
    private EnemySpawnValues thisRoundSpawn;

    public EnemyAI_Base meleeNormalPrefab;
    public EnemyAI_Base meleeTankPrefab;
    public EnemyAI_Base rangedNormalPrefab;
    public EnemyAI_Base rangedTankPrefab;
    public EnemyAI_Base bombNormalPrefab;

    public Transform currentEnemies;

    public RoundLogic roundLogic;

    public int RoundEnemyReserveCount { get { return enemySpawnQueue.Count; } }
    public int roundTotalEnemyCount;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemies", 0, 1f);
    }

    public void InitializeRound(EnemySpawnValues enemySpawnValues)
    {
        thisRoundSpawn = enemySpawnValues;
        allEnemies = new List<KeyValuePair<EnemyAI_Base, int>>();

        AddEnemy(thisRoundSpawn.meleeNormal, meleeNormalPrefab);
        AddEnemy(thisRoundSpawn.meleeTank, meleeTankPrefab);
        AddEnemy(thisRoundSpawn.rangedNormal, rangedNormalPrefab);
        AddEnemy(thisRoundSpawn.rangedTank, rangedTankPrefab);
        AddEnemy(thisRoundSpawn.bombNormal, bombNormalPrefab);

        KeyValuePair<EnemyAI_Base, int> [] allEnemiesArray = allEnemies.ToArray();
        CommonFunctions.Shuffle<KeyValuePair<EnemyAI_Base, int>>(allEnemiesArray);
        enemySpawnQueue = CommonFunctions.ArrayToQueue<KeyValuePair<EnemyAI_Base, int>>(allEnemiesArray);
        roundTotalEnemyCount = enemySpawnQueue.Count;
    }

    private void AddEnemy(EnemySpawnValues.EnemyDetails enemyDetail, EnemyAI_Base enemyPrefab)
    {
        for (int index = 0; index < enemyDetail.count; index++)
        {
            KeyValuePair<EnemyAI_Base, int> enemyData = new KeyValuePair<EnemyAI_Base, int>(enemyPrefab, enemyDetail.level);
            allEnemies.Add(enemyData);
        }
    }

    public void SpawnEnemies()
    {
        if (GameManager.Instance.roundStarted)
        {
            while (GameManager.Instance.allEnemies.Count < thisRoundSpawn.simultaneousEnemiesThisRound)
            {
                if (enemySpawnQueue.Count > 0)
                {
                    KeyValuePair<EnemyAI_Base, int> enemy = enemySpawnQueue.Dequeue();
                    GameObject enemyObject = GameObject.Instantiate(enemy.Key.gameObject, currentEnemies);
                    enemyObject.GetComponent<EnemyAI_Base>().SetLevel(enemy.Value);

                    if (Random.Range(0, 2) == 1)
                    {
                        Vector3 newPos = Camera.main.ViewportToWorldPoint(new Vector3(0.5f + (0.6f * CommonFunctions.PositiveOrNegative()), Random.Range(0, 1.1f), Camera.main.transform.position.z));
                        enemyObject.transform.position = new Vector3(newPos.x, newPos.y, 0);
                    }
                    else
                    {
                        Vector3 newPos = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0, 1.1f), 0.5f + (0.6f * CommonFunctions.PositiveOrNegative()), Camera.main.transform.position.z));
                        enemyObject.transform.position = new Vector3(newPos.x, newPos.y, 0);
                    }
                }
                else
                {
                    if (GameManager.Instance.allEnemies.Count == 0)
                    {
                        roundLogic.EndRound();
                    }
                    return;
                }
            }
        }
    }
}
