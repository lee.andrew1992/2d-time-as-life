﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class EnemyAI_Melee : EnemyAI_Base
{
    [HeaderAttribute("Melee Specific")]
    public float chargeDuration;
    public float chargeSpeed;
    public float chargeCoolDown;

    private float nextCharge;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate();
    }

    void FixedUpdate()
    {
        AliveCheck();
        MoveToTargetAndAttack();
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    protected override IEnumerator Attack()
    {
        if (nextCharge > Time.time)
        {
            yield return null;
        }
        else
        { 
            nextCharge = Time.time + chargeCoolDown;
            Vector2 chargeDirection = (target.position - this.transform.position).normalized;
            float currChargeDuration = 0;
            rb.AddForce(chargeDirection * chargeSpeed * speed, ForceMode2D.Force);

            while (currChargeDuration < chargeDuration)
            {
                attacking = true;
                currChargeDuration += Time.deltaTime;
                yield return null;
            }
            attacking = false;
        }
    }

    protected override void Instantiate()
    {
        base.Instantiate();
        nextCharge = 0;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (attacking)
        {
            if (collision.collider.gameObject.layer == target.gameObject.layer)
            {
                Kamikaze(target.gameObject);
            }
            else
            {
                this.StopAllCoroutines();
                attacking = false;
            }
        }
    }

    void Kamikaze(GameObject target)
    {
        target.GetComponent<CharacterEntity_Player>().TakeDamage(attackDamage, rb.velocity.normalized);
        this.GetComponent<CharacterEntity>().TakeDamage(NumericalValues.EnemyValues.Kamikaze.Damage, rb.velocity.normalized);
    }

    public override void SetLevel(int level)
    {
        this.level = Mathf.Min(level, NumericalValues.EnemyValues.MaxLevel);
        base.SetLevel(level);

        speed *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        chargeCoolDown *= 1f - ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        attackRange *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
    }
}
