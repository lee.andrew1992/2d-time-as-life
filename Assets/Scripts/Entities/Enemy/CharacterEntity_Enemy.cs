﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;
using TMPro;

public class CharacterEntity_Enemy : CharacterEntity
{
    [HeaderAttribute("Points System")]
    public int basePoints;

    [HeaderAttribute("Audio Source")]
    public SoundEffect deathSound;
    public SoundEffect hitSound;

    void Awake()
    {
        GameManager.Instance.SetEnemy(this);
        deathSound = new SoundEffect(deathSound.source);

        if (hitSound.source != null)
        { 
            hitSound = new SoundEffect(hitSound.source);
        }
    }

    public override void TakeDamage(int damage, Vector2 hitDirection)
    {

        if (damage == NumericalValues.EnemyValues.Kamikaze.Damage)
        {
            DieKamikaze();
        }
        else
        {
            if (hitSound.source != null)
            {
                hitSound.Play(Random.Range(0.9f, 1.1f));
            }
            base.TakeDamage(damage, hitDirection);
        }
    }

    private void DieKamikaze()
    {
        if (!alive)
        {
            return;
        }
        base.Die();

        GameManager.Instance.RemoveEnemy(this);
        GameObject.Destroy(radarIndicator);
    }

    public override void Die()
    {
        deathSound.Play(Random.Range(0.9f, 1.1f));

        if (!alive)
        {
            return;
        }
        base.Die();

        GameManager.Instance.thisPlayThroughEnemyKilledCount++;

        if (GameManager.Instance.timePickUpSpawnLogic.readyToSpawn && GameManager.Instance.allTimePickUps.Count == 0 && InPlayArea())
        {
            GameManager.Instance.timePickUpSpawnLogic.Spawn(this.transform.position);
        }

        AwardPoints(this.GetComponent<EnemyAI_Base>().level);

        GameManager.Instance.RemoveEnemy(this);
        GameObject.Destroy(radarIndicator);
    }

    public void AwardPoints(int level = 1)
    {
        int points = basePoints * level;
        GameManager.Instance.pointsSystem.AddPoints(points);

        GameObject enemyDeathAlert = GameManager.Instance.responsiveGUI.CreateUI(ResponsiveGUIManager.AlertType.EnemyPoints, (Vector2)this.transform.position, CalculateDeathAlertPositionOffset());
        enemyDeathAlert.GetComponent<ResponsiveUIController>().SetText((points * GameManager.Instance.pointsSystem.CurrentMultiplier).ToString());

        TextMeshProUGUI text = enemyDeathAlert.GetComponent<TextMeshProUGUI>();
        Color currColor = GameManager.Instance.comboSystem.comboColors[GameManager.Instance.comboSystem.GetComboColorIndex()];
        CommonFunctions.SetTMPColor(text, currColor);
        GameManager.Instance.comboSystem.AddToComboCounter();
    }

    public bool InPlayArea()
    {
        bool withinX = this.transform.position.x > GameManager.Instance.mapArea.bounds.min.x && this.transform.position.x < GameManager.Instance.mapArea.bounds.max.x;
        bool withinY = this.transform.position.y > GameManager.Instance.mapArea.bounds.min.y && this.transform.position.y < GameManager.Instance.mapArea.bounds.max.y;

        return withinX && withinY;
    }

    private Vector2 CalculateDeathAlertPositionOffset()
    {
        Vector2 direction = (GameManager.Instance.player.transform.position - this.transform.position).normalized;

        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            return (direction.x > 0 ? Vector2.left : Vector2.right) * 0.5f;
        }
        else
        {
            return (direction.y > 0 ? Vector2.down : Vector2.up) * 0.3f;
        }
    }
}
