﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class BombAttackLogic : MonoBehaviour
{
    public SpriteRenderer modelRenderer;
    public SpriteRenderer outerRing;
    public int damage;
    public bool targetPlayer;

    public Collider2D outerCircle;
    public Collider2D innerCircle;

    private bool playerHit;

    public void DoAttack(float attackDuration, float spreadSpeed, int damage, Color mainColor, Color fadeColor, bool hitPlayer = false)
    { 
        this.damage = damage;
        StartCoroutine(InitiateAttack(attackDuration, spreadSpeed, mainColor, fadeColor));
        targetPlayer = hitPlayer;
    }

    public IEnumerator InitiateAttack(float attackDuration, float spreadSpeed, Color mainColor, Color fadeColor)
    {
        this.transform.localScale = Vector3.zero;
        float currAttackDuration = 0;
        int obstaclesHit = 0;
        bool obstacleHit = false;
        modelRenderer.material.SetColor(StringValues.ShaderVariable.Common.MainColor, mainColor);
        modelRenderer.material.SetColor(StringValues.ShaderVariable.Dissolve.FadeColor, fadeColor);

        outerRing.material.SetColor(StringValues.ShaderVariable.Common.MainColor, mainColor);
        outerRing.material.SetColor(StringValues.ShaderVariable.Dissolve.FadeColor, fadeColor);
        bool playerHit = false;

        while (currAttackDuration < attackDuration)
        {
            currAttackDuration += Time.deltaTime;
            this.transform.localScale += Vector3.one * Time.deltaTime * spreadSpeed;
            modelRenderer.material.SetFloat(StringValues.ShaderVariable.Dissolve.Fade, modelRenderer.material.GetFloat(StringValues.ShaderVariable.Dissolve.Fade) - Time.deltaTime / attackDuration);

            foreach (MapCube cube in GameManager.Instance.mapController.mapCubes)
            {
                if (CommonFunctions.ObjectInColliderArea(cube.gameObject, outerCircle))
                {
                    CommonFunctions.DisableCube(cube, 1f, out obstacleHit);

                    if (obstacleHit)
                    {
                        obstaclesHit++;
                    }
                }
            }

            if (!targetPlayer)
            {
                HitEnemies();
                HitProjectiles();
            }
            else
            {
                if (!playerHit)
                {
                    HitPlayer(out playerHit);
                }
            }

            yield return null;
        }

        if (obstaclesHit > 0)
        {
            GameManager.Instance.ResetPathFindingGraph();
        }

        Destroy(this.gameObject);
    }

    void HitPlayer(out bool hit)
    {
        hit = false;

        if (CommonFunctions.ObjectInColliderArea(GameManager.Instance.player.gameObject, outerCircle))
        {
            if (!CommonFunctions.ObjectInColliderArea(GameManager.Instance.player.gameObject, innerCircle))
            {
                if (GameManager.Instance.player.gameObject.layer != NumericalValues.Layers.Invincible)
                {
                    Vector2 hitDirection = (this.transform.position - GameManager.Instance.player.transform.position);
                    GameManager.Instance.player.TakeDamage(damage, hitDirection.normalized);

                    hit = true;
                }
            }
        }
    }

    void HitProjectiles()
    {
        foreach (Transform bullet in GameManager.Instance.enemyBullets)
        {
            if (CommonFunctions.ObjectInColliderArea(bullet.gameObject, outerCircle))
            {
                bullet.GetComponent<ProjectileLogic>().KillBullet();
            }
        }
    }

    void HitEnemies()
    {
        CharacterEntity_Enemy [] currentAllEnemies = GameManager.Instance.allEnemies.ToArray();

        foreach (CharacterEntity_Enemy enemy in currentAllEnemies)
        {
            if (CommonFunctions.ObjectInColliderArea(enemy.gameObject, outerCircle))
            {
                enemy.TakeDamage(1000, Vector2.zero);
            }
        }
    }
}
