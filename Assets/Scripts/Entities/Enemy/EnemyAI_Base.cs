﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using ConstantValues;

public class EnemyAI_Base : MonoBehaviour
{
    public int level;
    public Transform target;
    public float speed;
    public float nextWayPointDistance;
    public Transform attackPoint;
    public int attackDamage;
    public float attackRange;

    protected Path path;
    protected int currentWayPointIndex = 0;
    protected bool attacking;

    protected Seeker seeker;
    protected Rigidbody2D rb;
    protected CharacterEntity entity;

    protected const float SCALE_PULSE_DURATION = 0.75f;

    protected virtual void Instantiate()
    {
        target = GameManager.Instance.player.transform;
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        entity = GetComponent<CharacterEntity>();

        InvokeRepeating("UpdatePath", 0, 0.6f);
        InvokeRepeating("ScalePulseFrame", 0, SCALE_PULSE_DURATION);
    }

    protected void MoveToTargetAndAttack()
    {
        if (path == null || attacking)
        {
            return;
        }

        if (currentWayPointIndex >= path.vectorPath.Count)
        {
            return;
        }
        if (Vector2.Distance(rb.position, target.position) < attackRange && (entity as CharacterEntity_Enemy).InPlayArea())
        {
            StartCoroutine(Attack());
        }
        else
        {
            Vector2 direction = ((Vector2)path.vectorPath[currentWayPointIndex] - rb.position).normalized;
            Vector2 force = direction * speed * Time.deltaTime;

            rb.AddForce(force);

            float distance = Vector2.Distance(rb.position, path.vectorPath[currentWayPointIndex]);

            if (distance < nextWayPointDistance)
            {
                currentWayPointIndex++;
            }
        }
    }

    protected void UpdatePath()
    {
        if (seeker.IsDone())
        { 
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    protected void OnPathComplete(Path newPath)
    {
        if (!newPath.error)
        {
            path = newPath;
            currentWayPointIndex = 0;
        }
    }

    protected virtual IEnumerator Attack()
    {
        yield return null;
    }

    protected void AliveCheck()
    {
        if (!entity.alive)
        {
            StopAllCoroutines();
            this.enabled = false;
            this.GetComponent<Collider2D>().enabled = false;
            return;
        }
    }

    public virtual void SetLevel(int level)
    {
        this.level = level;
    }

    public virtual void ScalePulseFrame()
    {
        StartCoroutine(CommonFunctions.ScalePulse(entity.frame.transform, SCALE_PULSE_DURATION, 0.2f, Vector3.one));
    }
}
