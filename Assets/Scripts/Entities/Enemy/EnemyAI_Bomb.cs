﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class EnemyAI_Bomb : EnemyAI_Base
{
    [HeaderAttribute("Bomb Specific")]
    public GameObject bombEffect;
    public float attackCoolDown;
    public float bombAttackDuration;
    public float attackDelay;
    public float bombSpreadSpeed;

    [ColorUsage(true, true)]
    public Color bombMainColor;

    [ColorUsage(true, true)]
    public Color bombFadeColor;

    public Material normalMat;
    public Material attackWarnMat;
    
    private float nextAttack;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate();
    }

    void FixedUpdate()
    {
        AliveCheck();
        MoveToTargetAndAttack();
    }

    protected override IEnumerator Attack()
    {
        if (nextAttack > Time.time)
        {
            yield return null;
        }
        else
        {
            float currAttackDelay = 0;
            ChangeMat(attackWarnMat);

            while (currAttackDelay < attackDelay)
            {
                currAttackDelay += Time.deltaTime;
                attacking = true;
                yield return null;
            }

            ChangeMat(normalMat);
            nextAttack = Time.time + attackCoolDown;
            GameObject bomb = GameObject.Instantiate(bombEffect, GameManager.Instance.enemyBombs);
            bomb.transform.position = this.transform.position;
            bomb.GetComponent<BombAttackLogic>().DoAttack(bombAttackDuration, bombSpreadSpeed, attackDamage, bombMainColor, bombFadeColor, true);

            attacking = false;
        }

    }

    protected override void Instantiate()
    {
        base.Instantiate();
        nextAttack = 0;
    }

    public override void SetLevel(int level)
    {
        this.level = Mathf.Min(level, NumericalValues.EnemyValues.MaxLevel);
        base.SetLevel(level);

        bombAttackDuration *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        bombSpreadSpeed *= 1f + ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        attackCoolDown *= 1f - ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
        attackDelay *= 1f - ((float)(level - 1) / NumericalValues.EnemyValues.MaxLevel);
    }

    private void ChangeMat(Material mat)
    {
        this.entity.frame.GetComponent<SpriteRenderer>().material = mat;
        this.entity.coreModel.GetComponent<SpriteRenderer>().material = mat;
    }
}
