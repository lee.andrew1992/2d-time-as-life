﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class Entity : MonoBehaviour
{
    protected GameObject radarIndicator;
    public GameObject RadarIndicator { get { return radarIndicator; } set { radarIndicator = value; } }

    public int health;
    public ParticleSystem hitImpact;

    public virtual void TakeDamage(int damage, Vector2 hitDirection, GameObject source)
    {
        if (hitImpact != null)
        {
            CommonFunctions.CreateParticleSystem(hitImpact, source.GetComponent<Renderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor), this.transform);
        }
        TakeDamage(damage, hitDirection);
    }

    public virtual void TakeDamage(int damage, Vector2 hitDirection, ParticleSystem source)
    {
        if (hitImpact != null)
        {
            if (!CommonFunctions.OutsideCamera(Camera.main, this.transform.position, Vector2.one * NumericalValues.RenderDistanceMaxMultiplier.EnemyHitImpact))
            {            
                CommonFunctions.CreateParticleSystem(hitImpact, source.GetComponent<ParticleSystemRenderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor), this.transform);
            }
        }

        TakeDamage(damage, hitDirection);
    }

    public virtual void TakeDamage(int damage, Vector2 hitDirection)
    {
        if (health < 0)
        {
            return;
        }

        health -= damage;
    }

    public virtual void Die()
    {
        this.GetComponent<Collider2D>().enabled = false;
    }
}
