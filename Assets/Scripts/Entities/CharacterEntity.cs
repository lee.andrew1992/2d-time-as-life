﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CharacterEntity : Entity
{
    public GameObject coreModel;
    public GameObject frame;
    public bool alive;

    public ParticleSystem deathEffect;
    public float deathFadeSpeed;

    public override void TakeDamage(int damage, Vector2 hitDirection)
    {
        base.TakeDamage(damage, hitDirection);

        if (health <= 0)
        {
            if (deathEffect != null)
            {
                DeathEffect(hitDirection);
            }
            Die();
        }
    }

    public override void Die()
    {
        base.Die();
        alive = false;

        if (frame != null)
        {
            frame.gameObject.SetActive(false);
        }

        StartCoroutine(CommonFunctions.Dissolve(coreModel.GetComponent<SpriteRenderer>().material, deathFadeSpeed, this.gameObject));
    }

    private void DeathEffect(Vector2 hitDirection)
    {
        ParticleSystem effect = CommonFunctions.CreateParticleSystem(deathEffect, coreModel.GetComponent<Renderer>().material.GetColor(StringValues.ShaderVariable.Common.MainColor), this.transform);

        ParticleSystem.MainModule effectMain = effect.main;
        effectMain.startRotationX = Quaternion.LookRotation(hitDirection).eulerAngles.x;
        effectMain.startRotationY = Quaternion.LookRotation(hitDirection).eulerAngles.y;
        effectMain.startRotationZ = Quaternion.LookRotation(hitDirection).eulerAngles.z;
        effect.transform.localScale = effect.transform.localScale * Random.Range(0.75f, 1.25f);

        effect.transform.LookAt(this.transform.position + new Vector3(hitDirection.x, hitDirection.y));
    }
}
