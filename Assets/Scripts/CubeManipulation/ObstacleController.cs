﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObstacleController : MonoBehaviour
{
    public int minimumRound;
    public int trapCountIncrementRoundCount;
    public int maxSimultaneousObstacles;
    public float obstacleSpawnLimitPercentage;

    public float createObstacleInterval;
    private float createObstacleTimer;

    CreateTraps trapCreator;
    CreateWalls wallCreator;

    private bool obstacleCreatedThisRound = false;
    public bool ObstacleCreatedThisRound { get { return obstacleCreatedThisRound; } set { obstacleCreatedThisRound = value; } }

    public List<MapCube> wallCubes;

    enum ObstacleType
    { 
        Traps = 0,
        Walls = 1
    }

    // Start is called before the first frame update
    void Start()
    {
        trapCreator = GameManager.Instance.trapCreator;
        wallCreator = GameManager.Instance.wallCreator;
        wallCubes = new List<MapCube>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O) && GameManager.Instance.pcDemoMode)
        {
            CreateObstacle();
        }

        if (GameManager.Instance.roundStarted)
        { 
            createObstacleTimer += Time.deltaTime;
        }

        if (createObstacleTimer > createObstacleInterval && !obstacleCreatedThisRound)
        { 
            // check if current round has enough enemies to last
            if (GameManager.Instance.enemySpawner.RoundEnemyReserveCount > 0 &&
                GameManager.Instance.enemySpawner.RoundEnemyReserveCount + GameManager.Instance.enemySpawner.currentEnemies.childCount 
                    < GameManager.Instance.enemySpawner.roundTotalEnemyCount * obstacleSpawnLimitPercentage)
            {
                CreateObstacle();
                obstacleCreatedThisRound = true;
                createObstacleTimer = 0;
            }
        }
    }

    public void CreateObstacle()
    {
        if (GameManager.Instance.currentRound < minimumRound)
        {
            return;
        }

        Array allObstacleTypes = Enum.GetValues(typeof(ObstacleType));
        ObstacleType randomObsType = (ObstacleType)allObstacleTypes.GetValue(UnityEngine.Random.Range(0, allObstacleTypes.Length));

        int randomIndex = 0;
        int totalTrapCount = Mathf.Max(Mathf.Min(GameManager.Instance.currentRound / trapCountIncrementRoundCount, maxSimultaneousObstacles), 1);

        HashSet<int> selectedObstacleIndex = new HashSet<int>();

        for (int index = 0; index < totalTrapCount; index++)
        { 
            switch (randomObsType)
            {
                case ObstacleType.Traps:
                    randomIndex = UnityEngine.Random.Range(0, trapCreator.presets.Length);
                    while (selectedObstacleIndex.Contains(randomIndex))
                    {
                        randomIndex = UnityEngine.Random.Range(0, trapCreator.presets.Length);
                    }
                    trapCreator.MakeTraps(randomIndex);
                    break;

                case ObstacleType.Walls:
                    randomIndex = UnityEngine.Random.Range(0, wallCreator.presets.Length);
                    while (selectedObstacleIndex.Contains(randomIndex))
                    {
                        randomIndex = UnityEngine.Random.Range(0, wallCreator.presets.Length);
                    }
                    wallCreator.MakeWalls(randomIndex);
                    break;
            }

            selectedObstacleIndex.Add(randomIndex);
        }
    }
}
