﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CreateWalls : CubeManipulator
{
    void Start()
    {
        affectedMapCubes = new List<MapCube>();
        activatedPresets = new List<GameObject>();
    }

    void Update()
    {
        if (GameManager.Instance.pcDemoMode && Input.GetKeyDown(KeyCode.W))
        {
            MakeWalls(1);
        }
    }

    public void MakeWalls(int preset)
    {
        if (preset < 0 || preset >= presets.Length)
        {
            return;
        }

        affectedMapCubes.Clear();
        ActivatePreset(preset);
        DoTransition(MapCube.TransitionTo.Wall);
        StartCoroutine(RecalculatePathFinding());
    }

    private IEnumerator RecalculatePathFinding()
    {
        yield return new WaitForSeconds(delay + 0.1f);
        GameManager.Instance.ResetPathFindingGraph();
    }
}
