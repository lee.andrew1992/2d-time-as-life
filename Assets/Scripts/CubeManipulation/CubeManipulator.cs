﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeManipulator : MonoBehaviour
{
    public float delay;
    protected List<MapCube> affectedMapCubes;
    protected const float WARNING_SHAKE_VARIATION = 0.02f;
    public GameObject[] presets;

    protected List<GameObject> activatedPresets;

    [ColorUsage(true, true)]
    public Color warningColor;
    [ColorUsage(true, true)]
    public Color changedColor;

    public void ActivatePreset(int presetIndex)
    {
        GameObject currPreset = GameObject.Instantiate(presets[presetIndex], this.transform);
        activatedPresets.Add(currPreset);
        AddToAffectedCubes(currPreset.transform);
    }

    private void AddToAffectedCubes(Transform objectTransform)
    {
        foreach (Transform child in objectTransform)
        {
            GetCubesInArea(child.GetComponent<Collider2D>());

            if (child.childCount > 0)
            {
                AddToAffectedCubes(child);
            }
        }
    }

    public void GetCubesInArea(Collider2D collider)
    {
        foreach (MapCube cube in GameManager.Instance.mapController.mapCubes)
        {
            if (CommonFunctions.ObjectInColliderArea(cube.gameObject, collider))
            {
                affectedMapCubes.Add(cube);
            }
        }
    }

    public void DoTransition(MapCube.TransitionTo transitionTo)
    {
        foreach (MapCube cube in affectedMapCubes)
        {
            if (!cube.isDisabled)
            { 
                cube.Transition(transitionTo, delay, WARNING_SHAKE_VARIATION, warningColor, changedColor);
            }
        }
    }
}
