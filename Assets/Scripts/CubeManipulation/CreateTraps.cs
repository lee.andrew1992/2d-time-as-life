﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConstantValues;

public class CreateTraps : CubeManipulator
{
    void Start()
    {
        affectedMapCubes = new List<MapCube>();
        activatedPresets = new List<GameObject>();
    }

    void Update()
    {
        if (GameManager.Instance.pcDemoMode && Input.GetKeyDown(KeyCode.T))
        {
            MakeTraps(1);
        }
    }

    public void MakeTraps(int preset)
    {
        if (preset < 0 || preset >= presets.Length)
        {
            return;
        }

        affectedMapCubes.Clear();
        ActivatePreset(preset);
        DoTransition(MapCube.TransitionTo.Trap);
    }
}
